: <<COMMENTBLOCK
Define names & paths of critical files & directories in the subject dir:
You should either name your files and structure your directories to match
the sample OR change this file to point to correctly named files and
directories for your dataset.
To change the file, you should modify only the right side (after the =)
of the export statements. This is because the left side of the export
statement defines the variable names used throughout the scripts.

The default naming stucture uses good informatic practices in that files
are 1) Labeled with the subject number (presumed to be the directory name).
and 2) Named from general to particular which sorts nicely when listed.

COMMENTBLOCK

########## FILE DESCRIPTOR SETTINGS #################
# If you have fieldmaps, then FM_exist=Yes, else No
# If you have reverse phase encode data (e.g., A-P and P-A acquisitions),
# then TOP_exist=Yes, else No
# If you have a lesion map, then LESION_exist=Yes, else No
export FM_exist=Yes
export LESION_exist=No
export TOP_exist=No

############## PREFERRED VIEWER ######################
export VIEW=fsleyes # the newest viewer
#export VIEW=fslview_deprecated #for those who have fsleyes, but don't like it
#export VIEW=fslview #for versions of fsl that don't have fsleyes installed
######################################################
################## PARAMETER FILES ####################
# Define parmeters and parameter files needed for fieldmaps and topup.

if [ ${FM_exist} = "Yes" ]; then
  # This is the Siemens default for the difference in echo times.
  # See readme for details of how it is calculated.
  export ECHO_DIFF=2.46
  # This is the effective echo spacing. It is needed to use epi_reg with
  # fieldmaps. See Readme for calculations.
  export ECHO_SPACE=0.00047
fi

source ${BIP}/PROFILES/bids_profile.sh
