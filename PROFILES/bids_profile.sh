: <<COMMENTBLOCK
Define names & paths of critical files & directories in the subject dir:
You should either name your files and structure your directories to match
the sample OR change this file to point to correctly named files and
directories for your dataset.
To change the file, you should modify only the right side (after the =)
of the export statements. This is because the left side of the export
statement defines the variable names used throughout the scripts.

The default naming stucture uses good informatic practices in that files
are 1) Labeled with the subject number (presumed to be the directory name).
and 2) Named from general to particular which sorts nicely when listed.

COMMENTBLOCK

SUBJECTDIR=`pwd`
SUBJECT=`basename $SUBJECTDIR`
PROJECT=`dirname $SUBJECTDIR`
source bip_functions.sh

#######################################################
################## PARAMETER FILES ####################
# Define parmeters and parameter files needed for fieldmaps and topup.

if [ ${FM_exist} = "Yes" ]; then
  # This is the Siemens default for the difference in echo times.
  # See readme for details of how it is calculated.
  export ECHO_DIFF=2.46
  # This is the effective echo spacing. It is needed to use epi_reg with
  # fieldmaps. See Readme for calculations.
  export ECHO_SPACE=0.00047
fi

if [ ${TOP_exist} = "Yes" ]; then
  # Parameter files for topup and applytopup:
  # These depend on the number of directions in your DWI and
  # Total Readout time for the Fieldmaps (see Readme.txt)
  export ACQP1=${PARAM}/acqp_AP.txt
  # for the B0_combo image, only 4 rows:
  export ACQPAR1=${PARAM}/acqparams.txt
  export IND1=${PARAM}/index_AP.txt
fi
########## START FILES ####################
# Define files that are expected in the subject directory
# Note that these are defined without any directory specification
# This is because they will get moved around.
export ANAT=${SUBJECT}_T1w
# We only want bvals and bvecs for the main 4D DWI image,
# even if a reverse phase encode image is present.
export BVECS=${SUBJECT}_dwi.bvec
export BVALS=${SUBJECT}_dwi.bval
export DWI_4D=${SUBJECT}_dwi

if [ ${FM_exist} = "Yes" ]; then
# Optional fieldmap files to improve distortion correction:
export MAG=${SUBJECT}_magnitude1
export PHASE=${SUBJECT}_phasediff
fi

if [ ${TOP_exist} = "Yes" ]; then
# If reverse phase encode images exist, then they replace DWI_4D,
# BVECS and BVALS above file does not exist
  export TOP1=${SUBJECT}_AP_dwi
# Top2 image is assumed to consist of B0 images only
  export TOP2=${SUBJECT}_PA_dwi
fi
############### DIR NAMES ###########################
# Define subject subdirs to be created and populated by
# setup.sh
export DERIV=${PROJECT}/derivatives/${SUBJECT}
export SRC=${PROJECT}/sourcedata/${SUBJECT}
export ARCHIVE=${SRC}/${SUBJECT}_archive
export ANAT_DIR=${SUBJECTDIR}/anat
export ANAT_DERIV=${DERIV}/anat
export DWI_DIR=${SUBJECTDIR}/dwi
export DWI_DERIV=${DERIV}/dwi

if [ ${FM_exist} = "Yes" ]; then
  export FM_DIR=${SUBJECTDIR}/fmap
  export FM_DERIV=${DERIV}/fmap
fi

export REG=${DERIV}/reg
export REGTEST=${DERIV}/regtest
export STATS=${DERIV}/stats
export SUBROIS=${DERIV}/subjectrois

### subdirectories ###
export BED=${DWI_DERIV}/Bed
export BEDPOST=${DWI_DERIV}/Bed.bedpostX
export BipS=${DWI_DERIV}/Bip_stand


############ INTERMEDIATE FILES #################
###### SPECIFIED WITH SUBDIR INCLUDED ###########
# These are cleaned up structural images:
export ANAT_BRAIN=${ANAT_DERIV}/${ANAT}_brain
#export ANAT_CROP=${ANAT_DERIV}/${ANAT}_cropped
export ANAT_CROP=${ANAT_DERIV}/${ANAT}_cropped_defaced
export ANAT_MASK=${ANAT_DERIV}/${ANAT}_mask
# This is the fsl_anat subdirectory:
export ANAT_SUB=${ANAT_DERIV}/${ANAT}.anat
# CSF, GM, and WM segmentations in structural space:
export CSF=${ANAT_DERIV}/${ANAT}_csfseg
export GM=${ANAT_DERIV}/${ANAT}_gmseg
export WM=${ANAT_DERIV}/${ANAT}_wmseg

# CSF, GM, and WM segmentations in B0 space:
export CSF_B0=${SUBROIS}/${SUBJECT}_B0_csfseg
export GM_B0=${SUBROIS}/${SUBJECT}_B0_gmseg
export WM_B0=${SUBROIS}/${SUBJECT}_B0_wmseg

# Intermediate fieldmap file:
if [ ${FM_exist} = "Yes" ]; then

  export RADS=${FM_DERIV}/${SUBJECT}_fmap_rads
fi

if [ ${LESION_exist} = "Yes" ]; then
  export LESION=${SUBJECT}_lesion
fi

# Intermediate DWI files:
  export B0=${DWI_DERIV}/${SUBJECT}_B0
  export B0_BRAIN=${DWI_DERIV}/${SUBJECT}_B0_brain
  export B0_MASK=${DWI_DERIV}/${SUBJECT}_B0_brain_mask
  # The output of eddy or eddy_correct
  export DWI_CORRECT=${DWI_DERIV}/${SUBJECT}_dwi_correct
  # Basename for dtifit output files
  # (FA, RD etc will be appended):
  export DFIT_OUT=${DWI_DERIV}/${SUBJECT}

if [ ${TOP_exist} = "Yes" ]; then
  export B0_TOP1=${DWI_DERIV}/${TOP1}_B0
  export B0_TOP2=${DWI_DERIV}/${TOP2}_B0
  # Combination of B0_TOP1 and B0_TOP2:
  export B0_COMBO=${DWI_DERIV}/${SUBJECT}_B0_combo
  # applytopup result (unwarped B0):
  # N.B. we extract a good B0 image to ${B0}
  # so topup and nontopup directories have the same name
  # for B0 files to bet and mask.
  export B0_HIFI=${DWI_DERIV}/${SUBJECT}_B0_hifi
  # A fieldmap file needed by topup:
  export TOP_COEF=${DWI_DERIV}/${SUBJECT}_topup_fieldcoef
  # The output warp basename for topup:
  export TOP_OUT=${DWI_DERIV}/${SUBJECT}_topup
fi

#################################################
############ REGISTRATIONS ######################
# N.B. Your registrations will be in ${REG}.
# These include linear registration matrices (*.mat) created with flirt
# and nonlinear warp registrations (*.nii.gz) created with fnirt
#
# epireg needs the phase encode direction. For A-P DWI data on the Siemens
# scanner this is -y. See BBR_DWI function called by prep.sh
# export PE=-y
# Linear FLIRT registration matrices for STR, B0:
export STR_B0=${REG}/${SUBJECT}_str_b0.mat
export B0_STR=${REG}/${SUBJECT}_b0_str

# Linear FLIRT registration matrices for MNI, STR:
export STR_MNI=${REG}/${SUBJECT}_str_mni.mat
export MNI_STR=${REG}/${SUBJECT}_mni_str.mat

# Linear FLIRT registration matrices for MNI, B0:
export B0_MNI=${REG}/${SUBJECT}_b0_mni.mat
export MNI_B0=${REG}/${SUBJECT}_mni_b0.mat

# FNIRT warp files for STR, B0:
export B0_STR_warp=${REG}/${SUBJECT}_b0_str_warp
export STR_B0_warp=${REG}/${SUBJECT}_str_b0_warp

# FNIRT warp files for MNI, STR
export STR_MNI_warp=${REG}/${SUBJECT}_str_mni_warp
export MNI_STR_warp=${REG}/${SUBJECT}_mni_str_warp

# FNIRT warp files for MNI, B0:
export B0_MNI_warp=${REG}/${SUBJECT}_b0_mni_warp
export MNI_B0_warp=${REG}/${SUBJECT}_mni_b0_warp
