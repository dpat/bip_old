#!/bin/bash
#.bip_profile takes an argument that sets the project specific profile.
# This way we can source the project specific profile with a variable name.

source ${PROJECT_PROFILE}
#==============================================================================

: <<COMMENTBLOCK

Function:   HelpMessage
Purpose:    Display help
Calls:      None

COMMENTBLOCK

HelpMessage ()
{
echo ""
echo "Set up your profile, e.g., bip_min_profile.sh"
echo "the profile indicates the presence of fieldmaps, reverse phase encode images or lesion masks"
echo "the profile also supplies the expected filenames, BIDS style names used by default"
echo ""
exit 1
}

#==============================================================================

: <<COMMENTBLOCK

Function:   Main
Purpose:    Run main functions
Input:      An acceptable set of files in a subject directory (run precheck.sh)
            Names can be set in your own project profile. See steps 1 and 2 in the Readme.
COMMENTBLOCK

Main ()
{
# Sort files into directories
SetupBipDir
#### Create masks and segmentations #######
# These functions only run if not already completed.
echo "starting PrepMPRAGE"
PrepMPRAGE
echo "starting PrepDWI"
PrepDWI
}
###### Optional Manual Masking #####
# At this point, people have the option of manually checking the masks and then
# running something like this: fslmaths dwi_image -mul revised_dwi_mask  dwi_brain
#==============================================================================
: <<COMMENTBLOCK
Extracts default B0 brain (may include distoriton correction if you have
reverse phase encode images and or field maps).  These maps should be checked.
This script will also extract and erode the magnitude image if it exists.
Finally, the anatomy image will be cropped, reoriented and segmented.
Registration warps are saved to the designated ${REG} directory.
COMMENTBLOCK

for arg in $*; do                               # For all commandline args
  case "$1" in                                  # Get the first arg
    -h|-help) HelpMessage;;                     # If arg=-h or -help, display help message
  esac                                          # End case
done                                            # End for loop

echo "Fieldmap? ${FM_exist}"
echo "Topup? ${TOP_exist}"
echo "Lesion? ${LESION_exist}"
Main
#==============================================================================
