#!/bin/bash
#.bip_profile takes an argument that sets the project specific profile.
# This way we can source the project specific profile with a variable name.

source ${PROJECT_PROFILE}
#==============================================================================

: <<COMMENTBLOCK

Function:   HelpMessage
Purpose:    Display help
Calls:      None

COMMENTBLOCK

HelpMessage ()
{
echo ""
echo "Set up your profile, e.g., bip_ak_rot_profile.sh"
echo "the profile indicates the presence of fieldmaps, reverse phase encode images or lesion masks"
echo "the profile also supplies the expected filenames, BIDS style names used by default"
echo ""
exit 1
}

#==============================================================================

: <<COMMENTBLOCK

Function:   Precheck
Purpose:    Determine whether the relevant files are available (and correctly named)
Calls:      Starting files must exist

COMMENTBLOCK

Precheck ()
{

echo "Your project profile is: ${PROJECT_PROFILE}"
echo "Your chosen viewer is: ${VIEW}"
echo "===================================="
echo "You are in this directory"
pwd
echo "the subject is ${SUBJECT}"
echo "===================================="
echo "These are the files you have"
ls -1
echo "===================================="
echo "The structural file should be:             ${ANAT}.nii.gz"
echo "===================================="

if [ ${TOP_exist} = "No" ]; then
  echo "4d diffusion should be:                   ${DWI_4D}.nii.gz"
  echo ""
  echo "the bvecs file should be:                 ${BVECS}"
  echo "the bvals file should be:                 ${BVALS}"
fi
echo "===================================="

if [ ${LESION_exist} = "Yes" ]; then
  echo "Lesion image should be:                   ${LESION}.nii.gz"
fi
echo "===================================="

if [ ${FM_exist} = "Yes" ]; then
  echo "The magnitude image should be:            ${MAG}.nii.gz"
  echo "and the phase image should be:            ${PHASE}.nii.gz"
  echo ""
  echo "The echo time difference default"
  echo "is 2.46 on Siemens"
  echo "Your echo time difference is set to:      ${ECHO_DIFF}"
  echo ""
  echo "Your echo spacing (for epi_reg) is:       ${ECHO_SPACE}"
fi
echo "===================================="

if [ ${TOP_exist} = "Yes" ]; then
  echo "TOP1 (primary DWI) should be:             ${TOP1}.nii.gz"
  echo "bvecs for top1 should be:                 ${BVECS}"
  echo "and bvals for top1 should be:             ${BVALS}"
  echo ""
  echo "TOP2 (reverse B0s) should be:             ${TOP2}.nii.gz"
#####I think I don't need bvals and bvecs for the second image, since it is just B0 images??####
#  echo "bvecs for top2 should be ${BVECS_TOP2}."
#  echo "bvals for top2 should be ${BVALS_TOP2}."
  # add more checking for the correct acquisition parameters
  # find only the first matching line (-m 1) , print arg2: this will get image dim
  echo "===================================="

  echo "These are the expected parameter files:"
  echo "                                            ${ACQP1}"
  echo "                                            ${ACQPAR1}"
  echo "                                            ${IND1}"
  echo "===================================="
  echo "These are the parameter files you have:"
  ls -1 ${PARAM}
  echo "===================================="
  DIM4=`fslinfo ${TOP1} | grep -m 1 dim4 | awk '{print $2}'`
  echo "Your ${TOP1} image has ${DIM4} volumes, therefore,"
  echo "your parameter files:${ACQP1} and ${IND1}"
  echo "should each have ${DIM4} lines."
fi

}

#==============================================================================
: <<COMMENTBLOCK
Before running any of these functions, make sure you have set up naming
in your project specific profile.sh and .bip_profile.

From bip_functions.sh call SetupBipDir to check for various files
and create the directory structure.
COMMENTBLOCK

for arg in $*; do                               # For all commandline args
  case "$1" in                                  # Get the first arg
    -h|-help) HelpMessage;;                     # If arg=-h or -help, display help message
  esac                                          # End case
done                                            # End for loop

echo "Fieldmap? ${FM_exist}"
echo "Topup? ${TOP_exist}"
echo "Lesion? ${LESION_exist}"
Precheck
echo "If the files are present, then run setup.sh"
##############
#==============================================================================
