#!/bin/bash
#.bip_profile takes an argument that sets the project specific profile.
# This way we can source the project specific profile with a variable name.

source ${PROJECT_PROFILE}
#==============================================================================
# defines variables, paths, and sources bip/LIBRARY/functions.sh
# includes everything from bip_img_profile.sh as well


: <<COMMENTBLOCK

###############################################################################
AUTHOR:         Dianne Patterson University of Arizona
DATE CREATED:   Today is 4/9/14. It is now 5:23 PM
----------------------------  DEPENDENCIES  -----------------------------------
This script assumes the dtifit, bedpostx and the registrations (dwi to MNI and
vice versa) have been run.  We also need the GM, WM, CSF segmentation from anat
space registered into dwi space.
===============================================================================
----------------------------  PURPOSE  ----------------------------------------
Prepare a subject directory for running bip scripts:
Generate a csf mask in diffusion space (if it does not exist)
Generate roi masks for each endpoint.
Generate an inverse mask to restrict the path of each tract.

===============================================================================
----------------------------  INPUT  ------------------------------------------
Which profile to use

===============================================================================
----------------------------  OUTPUT  -----------------------------------------
A diffusion space csf mask (MkCSFDTIMask)

Extract standard space rois that need to be translated into diffusion space.
This will differ by tract.  See MkSublistArray in bip_functions.sh.

Use this call to create a directory for the tract, e.g., DWI_DERIV/arc_l_bip
put the masks.txt (contents of array) in the directory.
For each of the named rois (-_csf extension), get it from standard location
and register it into diffusion space and put it in SUBROIS.
* There may be a need to do additional procedures on some standard rois once
they are in diffusion space.  e.g., may need to add some brocas regions together*

An inverse mask for each tract: /usr/local/tools/REF/IMAGES/STANDARD_MASKS/Exclusion
+ the csf mask - native space endpoint rois

===============================================================================
----------------------------  CALLS   -----------------------------------------
From other scripts MkSublistArray, MkDirIfNot, MyLog and MNI2B0Mask from bip_functions.sh;
bip_ireg2.sh functions must be run before bip_prep

###############################################################################

COMMENTBLOCK

###############################################################################
############################  DEFINE FUNCTIONS  ##################################

: <<COMMENTBLOCK

Function:   BuildATL_L
Purpose:    Import standard space rois for ATL,
            add and trim them in subject space.
Input:
Output:     ${SUBROIS}/atl_l_diff_bin
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildATL_L ()
{
Stand2Diff itg_ant_l mtg_ant_l stg_ant_l tp_l pHg_ant_l fusG_ant_l

fslmaths ${SUBROIS}/itg_ant_l_diff_bin -add ${SUBROIS}/mtg_ant_l_diff_bin \
-add ${SUBROIS}/stg_ant_l_diff_bin -add ${SUBROIS}/tp_l_diff_bin \
-add ${SUBROIS}/pHg_ant_l_diff_bin -add ${SUBROIS}/fusG_ant_l_diff_bin \
-bin ${SUBROIS}/atl_l_diff_bin -odt input

TrimEndpoints atl_l_diff_bin

}

#==============================================================================
: <<COMMENTBLOCK

Function:   BuildATL_R
Purpose:    Import standard space rois for ATL,
            add and trim them in subject space.
Input:      In subject diffusion space
Output:     ${SUBROIS}/atl_r_diff_bin
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildATL_R ()
{

Stand2Diff itg_ant_r mtg_ant_r stg_ant_r tp_r pHg_ant_r fusG_ant_r

fslmaths ${SUBROIS}/itg_ant_r_diff_bin -add ${SUBROIS}/mtg_ant_r_diff_bin \
-add ${SUBROIS}/stg_ant_r_diff_bin -add ${SUBROIS}/tp_r_diff_bin \
-add ${SUBROIS}/pHg_ant_r_diff_bin -add ${SUBROIS}/fusG_ant_r_diff_bin \
-bin ${SUBROIS}/atl_r_diff_bin -odt input

TrimEndpoints atl_r_diff_bin

}

#==============================================================================

: <<COMMENTBLOCK

Function:   BuildBrocas_L
Purpose:    Add ba44,45,47 in subject diffusion space to create brocas3
Input:      ba44,45,47 in subject diffusion space
Output:     ${SUBROIS}/brocas3_l_diff
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildBrocas_L ()
{
Stand2Diff ba44_l ba45_l ba47_l

fslmaths ${SUBROIS}/ba44_l_diff_bin -add ${SUBROIS}/ba45_l_diff_bin \
-add ${SUBROIS}/ba47_l_diff_bin -bin ${SUBROIS}/brocas3_l_diff_bin -odt input

TrimEndpoints brocas3_l_diff_bin

}

#==============================================================================

: <<COMMENTBLOCK

Function:   BuildBrocas_R
Purpose:    Add ba44,45,47 in subject diffusion space to create brocas3
Input:      ba44,45,47 in subject diffusion space
Output:     ${SUBROIS}/brocas3_r_diff
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildBrocas_R ()
{
Stand2Diff ba44_r ba45_r ba47_r

fslmaths ${SUBROIS}/ba44_r_diff_bin -add ${SUBROIS}/ba45_r_diff_bin \
-add ${SUBROIS}/ba47_r_diff_bin -bin ${SUBROIS}/brocas3_r_diff_bin -odt input

TrimEndpoints brocas3_r_diff_bin
}


#==============================================================================
<<COMMENTBLOCK

Function:   BuildBrocasClassic_L
Purpose:    Add ba44 and ba45 to create brocas_classic
Input:      Assumes ba44 and ba45 already exist in subject diffusion space
Output:     ${SUBROIS}/brocas_classic_l_diff_bin
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildBrocasClassic_L ()
{
Stand2Diff ba44_l ba45_l

fslmaths ${SUBROIS}/ba44_l_diff_bin -add ${SUBROIS}/ba45_l_diff_bin \
-bin ${SUBROIS}/brocas_classic_l_diff_bin -odt input

TrimEndpoints brocas_classic_l_diff_bin

}

#==============================================================================

<<COMMENTBLOCK

Function:   BuildBrocasClassic_R
Purpose:    Add ba44 and ba45 to create brocas_classic
Input:      Assumes ba44 and ba45 already exist in subject diffusion space
Output:     ${SUBROIS}/brocas_classic_r_diff_bin
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildBrocasClassic_R ()
{

Stand2Diff ba44_r ba45_r

fslmaths ${SUBROIS}/ba44_r_diff_bin -add ${SUBROIS}/ba45_r_diff_bin \
-bin ${SUBROIS}/brocas_classic_r_diff_bin -odt input

TrimEndpoints brocas_classic_r_diff_bin

}

#==============================================================================
: <<COMMENTBLOCK

Function:   BuildDir
Purpose:    Build Directory for tracking and put masks.txt file in it
Input:      standard tract names in a string e.g., blah_l blah_r
Output:     ${DWI_DERIV}/${tract}_bip
Calls:      MkSublistArray and MkDirIfNot from bip_functions.sh

COMMENTBLOCK

BuildDir ()
{

for tract in $@; do
  if [ ! -d  ${DWI_DERIV}/${tract}_bip ]; then
    MkSublistArray lst_tract_seed_target_masks.txt ${tract}
    MkDirIfNot ${DWI_DERIV}/${tract}_bip
    echo  "${sublist_array[1]}" > ${DWI_DERIV}/${tract}_bip/masks.txt
    echo  "${sublist_array[2]}" >> ${DWI_DERIV}/${tract}_bip/masks.txt
  fi
done
}

#==============================================================================
: <<COMMENTBLOCK

Function:   BuildEndpoints
Purpose:    Build diff space endpoint from standard
Input:      standard endpoints in a string: e.g., ba44_l ba45_l
Output:     ${SUBROIS}/${roi}_diff_bin
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildEndpoints ()
{
for roi in $@; do
  Stand2Diff ${roi}
  TrimEndpoints ${roi}_diff_bin
done
}

#==============================================================================
: <<COMMENTBLOCK

Function:   BuildInverse
Purpose:    Build diff space inverse mask from standard, add csf to mask
Input:      standard Inverse masks in a string e.g., blah_l blah_r
Output:     ${SUBROIS}/${tract}_inverse_mask_diff_bin_csf
Note:       In reaction to Cb-Th tract, I have switched the order of steps
            for building the inverse.  Now csf is added last which means it over rides the endpoint masks.  It probably always should have been this way, but has not been an issue
            until now. July 18, 2016
Calls:      MkSublistArray and MNI2B0Mask from bip_functions.sh;

COMMENTBLOCK

BuildInverse ()
{

for tract in $@; do
  if [ ! -e ${SUBROIS}/${tract}_inv_mask_diff_bin_csf.nii.gz ]; then
    MNI2B0MaskWarp ${INVERSE}/${tract}_inv_mask ${SUBROIS}/${tract}_inv_mask_diff
    MkSublistArray lst_tract_seed_target_masks.txt ${tract}
    fslmaths ${SUBROIS}/${tract}_inv_mask_diff_bin -sub ${SUBROIS}/${sublist_array[1]} \
    -sub ${SUBROIS}/${sublist_array[2]} -add ${CSF_B0}_bin \
    -bin ${SUBROIS}/${tract}_inv_mask_diff_bin_csf -odt input
    imrm ${SUBROIS}/${tract}_inv_mask_diff_bin
  else
    echo "${SUBROIS}/${tract}_inv_mask_diff_bin_csf.nii.gz already exists"
  fi
done
}

#==============================================================================
:
: <<COMMENTBLOCK

Function:   BuildMdlfPost_L
Purpose:    Add angular, mtl_post, stg, supramarginal in subject diffusion space
            to create mdlf_post
Input:      angular, mtg_post, stg, supramarginal in subject diffusion space
Output:     ${SUBROIS}/mdlf_post_l_diff
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildMdlfPost_L ()
{
Stand2Diff angular_l mtg_post_l stg_post_l supramarginal_l

fslmaths ${SUBROIS}/angular_l_diff_bin -add ${SUBROIS}/mtg_post_l_diff_bin \
-add ${SUBROIS}/stg_post_l_diff_bin -add ${SUBROIS}/supramarginal_l_diff_bin \
-bin ${SUBROIS}/mdlf_post_l_diff_bin -odt input

TrimEndpoints mdlf_post_l_diff_bin

fslmaths ${SUBROIS}/mdlf_post_l_diff_bin_csf -sub ${SUBROIS}/atl_l_diff_bin_csf \
-thr 0 -bin ${SUBROIS}/mdlf_post_l_diff_bin_csf -odt input

}

#==============================================================================
:
: <<COMMENTBLOCK

Function:   BuildMdlfPost_R
Purpose:    Add angular, mtl_post, stg, supramarginal in subject diffusion space
            to create mdlf_post
Input:      angular, mtg_post, stg, supramarginal in subject diffusion space
Output:     ${SUBROIS}/mdlf_post_r_diff
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildMdlfPost_R ()
{

Stand2Diff angular_r mtg_post_r stg_post_r supramarginal_r

fslmaths ${SUBROIS}/angular_r_diff_bin -add ${SUBROIS}/mtg_post_r_diff_bin \
-add ${SUBROIS}/stg_post_r_diff_bin -add ${SUBROIS}/supramarginal_r_diff_bin \
-bin ${SUBROIS}/mdlf_post_r_diff_bin -odt input

TrimEndpoints mdlf_post_r_diff_bin

fslmaths ${SUBROIS}/mdlf_post_r_diff_bin_csf -sub ${SUBROIS}/atl_r_diff_bin_csf \
-thr 0 -bin ${SUBROIS}/mdlf_post_r_diff_bin_csf -odt input
}

#==============================================================================
:
: <<COMMENTBLOCK

Function:   BuildSlf2Ant_L
Purpose:    Add frontal_mid and ba6and8 in subject diffusion space
            to create slf2_ant
Input:      frontal_mid and ba6and8 in subject diffusion space
Output:     ${SUBROIS}/slf2_ant_l_diff
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildSlf2Ant_L ()
{
Stand2Diff frontal_mid_l ba6and8_l

fslmaths ${SUBROIS}/frontal_mid_l_diff_bin -add ${SUBROIS}/ba6and8_l_diff_bin \
-bin ${SUBROIS}/slf2_ant_l_diff_bin -odt input

TrimEndpoints slf2_ant_l_diff_bin

}

#==============================================================================
:
: <<COMMENTBLOCK

Function:   BuildSlf2Ant_R
Purpose:    Add frontal_mid and ba6and8 in subject diffusion space
            to create slf2_ant
Input:      frontal_mid and ba6and8 in subject diffusion space
Output:     ${SUBROIS}/slf2_ant_r_diff
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildSlf2Ant_R ()
{
Stand2Diff frontal_mid_r ba6and8_r

fslmaths ${SUBROIS}/frontal_mid_r_diff_bin -add ${SUBROIS}/ba6and8_r_diff_bin \
-bin ${SUBROIS}/slf2_ant_r_diff_bin -odt input

TrimEndpoints slf2_ant_r_diff_bin
}
#==============================================================================
: <<COMMENTBLOCK

Function:   BuildWernickes_L
Purpose:    Add angular, mtl_post, stg, supramarginal in subject diffusion
            space to create Wernickes5
Input:      angular, mtl_post, stg, supramarginal in subject diffusion space
Output:     ${SUBROIS}/wernickes5_l_diff
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildWernickes_L ()
{
Stand2Diff angular_l mtg_post_l stg_l supramarginal_l

fslmaths ${SUBROIS}/angular_l_diff_bin -add ${SUBROIS}/mtg_post_l_diff_bin \
-add ${SUBROIS}/stg_l_diff_bin -add ${SUBROIS}/supramarginal_l_diff_bin \
-bin ${SUBROIS}/wernickes5_l_diff_bin -odt input

TrimEndpoints wernickes5_l_diff_bin
}

#==============================================================================
: <<COMMENTBLOCK

Function:   BuildWernickes_R
Purpose:    Add angular, mtl_post, stg, supramarginal in subject diffusion
            space to create Wernickes5
Input:      angular, mtl_post, stg, supramarginal in subject diffusion space
Output:     ${SUBROIS}/wernickes5_r_diff
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildWernickes_R ()
{
Stand2Diff angular_r mtg_post_r stg_r supramarginal_r

fslmaths ${SUBROIS}/angular_r_diff_bin -add ${SUBROIS}/mtg_post_r_diff_bin \
-add ${SUBROIS}/stg_r_diff_bin -add ${SUBROIS}/supramarginal_r_diff_bin \
-bin ${SUBROIS}/wernickes5_r_diff_bin -odt input

TrimEndpoints wernickes5_r_diff_bin
}

#==============================================================================

: <<COMMENTBLOCK

Function:   Stand2Diff
Purpose:    Convert standard space roi to diffusion space
Input:      Standard space roi in ${ROIS}
Output:     binary roi in subject diffusion space
Calls:      MNI2B0Mask from bip_functions.sh

COMMENTBLOCK

Stand2Diff ()
{
for roi in $@; do
  if [ ! -e ${SUBROIS}/${roi}_diff_bin.nii.gz ]; then
    MNI2B0MaskWarp ${ROIS}/${roi} ${SUBROIS}/${roi}_diff
  else
    echo "${SUBROIS}/${roi}_diff already exists"
  fi
done
}

#==============================================================================
: <<COMMENTBLOCK

Function:   TrimEndpoints
Purpose:    Trim a diff space roi with the csf_diff mask
Input:      roi_diff
Output:     roi_csf
Calls:      None

COMMENTBLOCK

TrimEndpoints ()
{
for roi in $@; do
  fslmaths ${SUBROIS}/${roi} -sub ${CSF_B0}_bin -thr 0 -bin ${SUBROIS}/${roi}_csf -odt input
  imrm ${SUBROIS}/${roi}
done
}

#==============================================================================
: <<COMMENTBLOCK

Function:   HelpMessage
Purpose:    Display help if user enters fewer than one argument
Input:      None
Output:     Help Message
Calls:      None

COMMENTBLOCK

HelpMessage ()
{
echo "Prepare a subject directory for running bip scripts:"
echo "Generate a csf mask in diffusion space (if it does not exist)"
echo "Generate roi masks for each endpoint."
echo "Generate an inverse mask to restrict the path of each tract."
echo "Runs Main with no arguments"
echo "Or can run an individual tract if that is passed as the argument"
echo "Possible tracts are:
  arc_l
  arc_r
  aslant_l
  aslant_r
  b3tob3
  cb_th_lr
  cb_th_rl
  extcap_l
  extcap_r
  ilf_l
  ilf_r
  iof_l
  iof_r
  mdlf_l
  mdlf_r
  slf2_l
  slf2_r
  unc_l
  unc_r
  vwfa2vwfa
  w5tow5"
exit 1
}

#==============================================================================
: <<COMMENTBLOCK

Function:   arc_l
Purpose:    prepare for running the left arcuate tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

arc_l ()
{
BuildBrocas_L
BuildWernickes_L
BuildInverse arc_l
BuildDir arc_l
}

#==============================================================================
: <<COMMENTBLOCK

Function:   arc_r
Purpose:    prepare for running the right arcuate tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

arc_r ()
{
BuildBrocas_R
BuildWernickes_R
BuildInverse arc_r
BuildDir arc_r
}
#==============================================================================
: <<COMMENTBLOCK

Function:   aslant_l
Purpose:    prepare for running the left aslant tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

aslant_l ()
{
BuildBrocasClassic_L
BuildEndpoints sma_l
BuildInverse aslant_l
BuildDir aslant_l
}

#==============================================================================
: <<COMMENTBLOCK

Function:   aslant_r
Purpose:    prepare for running the right aslant tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

aslant_r ()
{
BuildBrocasClassic_R
BuildEndpoints sma_r
BuildInverse aslant_r
BuildDir aslant_r
}

#==============================================================================
: <<COMMENTBLOCK

Function:   b3tob3
Purpose:    prepare for running the trans-callosal tract between brocas on the right and left.
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

b3tob3 ()
{
BuildBrocas_L
BuildBrocas_R
BuildInverse b3tob3
BuildDir b3tob3
}

#==============================================================================
: <<COMMENTBLOCK

Function:   cb_th_lr
Purpose:    prepare for running the tract between the left cerebellar hemisphere
            and the contralateral thalamus.
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

cb_th_lr ()
{
BuildEndpoints mni_Cb_l mni_thalamus_r
BuildInverse cb_th_lr
BuildDir cb_th_lr
}

#==============================================================================
: <<COMMENTBLOCK

Function:   cb_th_rl
Purpose:    prepare for running the tract between the right cerebellar hemisphere
            and the contralateral thalamus.
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

cb_th_rl ()
{
BuildEndpoints mni_Cb_r mni_thalamus_l
BuildInverse cb_th_rl
BuildDir cb_th_rl
}

#==============================================================================
: <<COMMENTBLOCK

Function:   extcap_l
Purpose:    prepare for running the left extreme capsule tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

extcap_l ()
{
BuildBrocas_L
BuildWernickes_L
BuildInverse extcap_l
BuildDir extcap_l
}

#==============================================================================
: <<COMMENTBLOCK

Function:   extcap_r
Purpose:    prepare for running the right extreme capsule tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

extcap_r ()
{
BuildBrocas_R
BuildWernickes_R
BuildInverse extcap_r
BuildDir extcap_r
}

#==============================================================================
: <<COMMENTBLOCK

Function:   ilf_l
Purpose:    prepare for running the left inferior longitudinal fasciculus tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

ilf_l ()
{
BuildATL_L
BuildEndpoints vwfa_l
BuildInverse ilf_l
BuildDir ilf_l
}

#==============================================================================
: <<COMMENTBLOCK

Function:   ilf_r
Purpose:    prepare for running the right inferior longitudinal fasciculus tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

ilf_r ()
{
BuildATL_R
BuildEndpoints vwfa_r
BuildInverse ilf_r
BuildDir ilf_r
}

#==============================================================================
: <<COMMENTBLOCK

Function:   iof_l
Purpose:    prepare for running the left inferior occipital fasciculus tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

iof_l ()
{
BuildEndpoints frontal_orbital_l occipital_l
BuildInverse iof_l
BuildDir iof_l
}

#==============================================================================
: <<COMMENTBLOCK

Function:   iof_r
Purpose:    prepare for running the right inferior occipital fasciculus tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

iof_r ()
{
BuildEndpoints frontal_orbital_r occipital_r
BuildInverse iof_r
BuildDir iof_r
}

#==============================================================================
: <<COMMENTBLOCK

Function:   mdlf_l
Purpose:    prepare for running the left middle longitudinal fasciculus tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

mdlf_l ()
{
BuildATL_L
BuildMdlfPost_L
BuildInverse mdlf_l
BuildDir mdlf_l
}

#==============================================================================
: <<COMMENTBLOCK

Function:   mdlf_r
Purpose:    prepare for running the right middle longitudinal fasciculus tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

mdlf_r ()
{
BuildATL_R
BuildMdlfPost_R
BuildInverse mdlf_r
BuildDir mdlf_r
}

#==============================================================================
: <<COMMENTBLOCK

Function:   slf2_l
Purpose:    prepare for running the left superior longitudinal fasciculus part 2 tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

slf2_l ()
{
BuildSlf2Ant_L
BuildEndpoints angular_l
BuildInverse slf2_l
BuildDir slf2_l
}


#==============================================================================
: <<COMMENTBLOCK

Function:   slf2_r
Purpose:    prepare for running the right superior longitudinal fasciculus part 2 tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

slf2_r ()
{
BuildSlf2Ant_R
BuildEndpoints angular_r
BuildInverse slf2_r
BuildDir slf2_r
}

#==============================================================================
: <<COMMENTBLOCK

Function:   unc_l
Purpose:    prepare for running the left uncinate tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

unc_l ()
{
BuildATL_L
BuildEndpoints frontal_orbital_l
BuildInverse unc_l
BuildDir unc_l
}

#==============================================================================
: <<COMMENTBLOCK

Function:   unc_r
Purpose:    prepare for running the right uncinate tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

unc_r ()
{
BuildATL_R
BuildEndpoints frontal_orbital_r
BuildInverse unc_r
BuildDir unc_r
}

#==============================================================================
: <<COMMENTBLOCK

Function:   vof_l
Purpose:    prepare for running the left vertical occipital fasciculus tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

vof_l ()
{
BuildEndpoints angular_l vwfa_l
BuildInverse vof_l
BuildDir vof_l
}

#==============================================================================
: <<COMMENTBLOCK

Function:   vof_r
Purpose:    prepare for running the right vertical occipital fasciculus tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

vof_r ()
{
BuildEndpoints angular_r vwfa_r
BuildInverse vof_r
BuildDir vof_r
}

#==============================================================================
: <<COMMENTBLOCK

Function:   vwfa2vwfa
Purpose:    prepare for running the trans-callosal tract between the visual
            word form areas on the left and right
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

vwfa2vwfa ()
{
BuildEndpoints vwfa_l vwfa_r
BuildInverse vwfa2vwfa
BuildDir vwfa2vwfa
}

#==============================================================================
: <<COMMENTBLOCK

Function:   w5tow5
Purpose:    prepare for running the trans-callosal tract between wernicke's area
            on the left and right
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

w5tow5 ()
{
BuildWernickes_L
BuildWernickes_R
BuildInverse w5tow5
BuildDir w5tow5
}

#==============================================================================
: <<COMMENTBLOCK

Function:   Main
Purpose:    Run the correct set of functions with appropriate arguments
Input:      Assumes existence of a bunch of files the functions need
Output:     Subject directory prepared for running bip scripts.

COMMENTBLOCK

Main ()
{

if [ ! -e ${SUBROIS}/${SUBJECT}_B0_csfseg_bin.nii.gz ]; then
 	echo "you must run check.sh Reg and ReviewCheckReg before continuing"
	echo 1
  else
  arc_l
  arc_r
  aslant_l
  aslant_r
  b3tob3
  cb_th_lr
  cb_th_rl
  extcap_l
  extcap_r
  ilf_l
  ilf_r
  iof_l
  iof_r
  mdlf_l
  mdlf_r
  slf2_l
  slf2_r
  unc_l
  unc_r
  vwfa2vwfa
  w5tow5
 fi
}

#########################  END FUNCTION DEFINITIONS  ##########################
###############################################################################

while getopts h options 2> /dev/null; do        # Setup -h flag. redirect errors
  case $options in                            			# In case someone invokes -h
  h) HelpMessage;;                            			# Run the help message function
  \?) echo "only h is a valid flag" 1>&2      	# If bad options are passed, print message
  esac
done

shift $(( $OPTIND -1))                        		 	# Index option count, so h isn't treated as arg
optnum=$(( $OPTIND -1))                         	# optnums hold # of options passed in

if [ ${optnum} -lt 1 -a $# -lt 1 ]; then        	# If no options
  dothis=Main                                        	# then first arg is function
else
  dothis=$1                           			 	# Execute requested function
fi

$dothis

MyLog                                           			      # Log script, args & date
