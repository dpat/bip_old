#!/bin/bash
# To create a probabalistic atlas, we must first convert a hotspot image to %
# Then we run threshold to create the expected maxprob images.

#paths
source img_profile.sh

if [ $# -lt 2 ]
then
        echo "Usage: $0 <subdir> <dim-in-mm>"
        echo "Example: $0 BIP 2"
        echo "create a 4d atlas image from a directory of hotspot images.  Specify the voxel dim (usually 1 or 2)"
        echo "This assumes you are in a directory containing all and only your hotspot images."
        echo "In addition, we create and populate the subdir, that together with the xml file"
        echo "must be moved to the FSL atlas directory.  fslview should have a new atlas with your subdir name."
        echo ""
        exit 1
fi

dim=$2
subdir=$1
probimage="${subdir}_prob-${dim}mm"
xmlfile="${subdir}.xml"

# Create the header info for the xml file
touch ${xmlfile}
# dump the basic header info into a file.  Note the image files are not yet correctly specified
echo '<?xml version="1.0" encoding="ISO-8859-1"?>' >>${xmlfile}
echo '<atlas version="1.0">' >>${xmlfile}
echo "<header>" >>${xmlfile}
echo "<name>${subdir}</name>" >>${xmlfile}
echo "<type>Probabalistic</type>" >>${xmlfile}
echo "<images>" >>${xmlfile}
echo "<imagefile>${subdir}/${probimage}</imagefile>" >>${xmlfile}
echo "<summaryimagefile>${subdir}/${subdir}_maxprob-thr25-2mm</summaryimagefile>" >>${xmlfile}
echo "</images>" >>${xmlfile}
echo "</header>" >>${xmlfile}
echo "<data>" >>${xmlfile}

n=0

# iterate through the images in the directory.  Create the % images and create the body of the xml file.
# The body of the xml file contains file names, indices and 0 values in the coordinates.
#
for i in *.nii.gz
do
    fatlas_sub.sh $i $dim
    image=`remove_ext $i`
    echo '<label index="'${n}'" x="0" y="0" z="0">'${image}'</label>' >> ${xmlfile}
    let n+=1
done

echo "</data>" >>${xmlfile}
echo "</atlas>" >>${xmlfile}

# Create the 4D probability image
fslmerge -t ${probimage} *_per-${dim}mm.nii.gz

# Produce the maxprob images
${FSLDIR}/data/atlases/bin/threshold ${probimage}

# Put the correct coordinates in the xml file
${FSLDIR}/data/atlases/bin/locate-centres ${probimage} ${xmlfile}

mkdir ${subdir}
mv *prob* ${subdir}

echo "You should move ${xmlfile} and ${subdir} to ${FSLDIR}/data/atlases"
MyLog
