#!/bin/bash
# To create a probabalistic atlas, we must first convert a hotspot image to %

#paths
source img_profile.sh

if [ $# -lt 2 ]
then
        echo "Usage: $0 <hotspot-image> <dim-in-mm>"
        echo "Example: $0 all_arc3_l_A_stand 2"
        echo "create an atlas compatible % image from a hotspot image.  Specify the voxel dim (usually 1 or 2)"
        echo ""
        exit 1
fi

# Use the built in fsl tool for removing image extensions
image=`remove_ext $1`
dim=$2

# find the highest value in the image
max=`fslstats ${image} -R | cut -d " " -f 2`
# Use bc to create the divisor needed to normalize the image to a scale of 100
divisor=`echo "scale=2; ${max}/100" | bc`
# Apply the divisor to the image
fslmaths ${image} -div ${divisor} ${image}_per-${dim}mm

MyLog

