The BIP atlas is based on my own iterative parcellation work.
Today is 3/6/14.

See lst_tract_seed_target_masks.txt and lst_build_rois.txt for more information about the details
of these tracts. Also see /usr/local/tools/REF/IMAGES/STANDARD_MASKS/Readme.txt for more details
about how each roi was created.
==========================
TRACT
LABEL: arc_l_tract and arc_r_tract
DESCRIPTION: arcuate fasciculus (arc).  left and right.

ROIA:
LABEL: arc_spt_l or arc_spt_r (temporal parcellation connected to the arcuate)
DESCRIPTION OF STARTING ROI: The temporal language area  subsumes five structures in
the left temporoparietal cortex: (1) angular gyrus, (BA39) “AG”; (2) anterior superior
temporal gyrus “ASTG”; (3) posterior middle temporal gyrus “PMTG”; (4) posterior
superior temporal gyrus “PSTG” and (5) supramarginal gyrus (BA40) “SMG”.

ROIB:
LABEL: arc_ifg_l and arc_ifg_r (frontal parcellation connected to the arcuate)
DESCRIPTION OF STARTING ROI: The frontal language area subsumes three structures in the
left inferior frontal gyrus (IFG): the (1) pars opercularis (BA 44), (2) pars
triangularis (BA 45) and (3) pars orbitalis (BA 47).

--------------------------
TRACT
LABEL: aslant_l_tract and aslant_r_tract
DESCRIPTION: aslant tract.  left and right.  See Catani, M., Mesulam, M. M., Jakobsen, E., Malik, F.,
Martersteck, A., Wieneke, C., et al. (2013). A novel frontal pathway underlies verbal fluency in
primary progressive aphasia. Brain, 136(8), 2619–2628.

ROIA:
LABEL: aslant_sma_l or aslant_sma_r (supplementary motor area)
 Following Catani 2013 on the aslant tract as closely as possible:
 # Add ant and post cing:
 >fslmaths ant_cing.nii.gz -add pos_cing.nii.gz cing.nii.gz
 # Make binary, (note, no 50% threshold…I liked the whole extent for this task):
 >fslmaths cing -bin cing_bin
 # Subtract cing from sma_lr after making sma_lr binary:
 (sma_lr comes from the Harvard-Oxford Cortical atlas, where it is now called the
 Juxtapositional Lobule Cortex (formerly Supplementary Motor Cortex))
 >fslmaths sma_lr -bin sma_lr_bin
 >fslmaths sma_lr_bin -sub cing_bin -bin sma_lr_stage1
 Also, simply remove anything left at or below Z=64 vox (56 mm):
 sma_lr_stage2
 This only picks out the most medial gyrus of the SGF of interest though…
 My ROI is only frontal, not parietal (that's good)…so it does NOT go too far posterior.
 All indications are that I should leave the A-P extent as it is:
 Y 72 (18 mm) anterior to Y=51 (-24 mm) posterior
 And manually add the 2nd gyrus. Call this sma_lr_stage3.
 The bi-lobar (2 gyrus) structure disappears posterior to Y=60 (-6mm),
 so I don't continue the addition of the 2nd gyrus after Y=60
 >fslmaths sma_lr_stage3 -mul ../Lobe_Hemi_Brain/hemisphere_r -bin sma_r
 >fslmaths sma_lr_stage3 -mul ../Lobe_Hemi_Brain/hemisphere_l -bin sma_l

ROIB:
LABEL: aslant_ifg_l and aslant_ifg_r (frontal parcellation connected to the arcuate)
DESCRIPTION OF STARTING ROI: The classic frontal language area subsumes two structures in the
left inferior frontal gyrus (IFG): the (1) pars opercularis (BA 44), and (2) pars
triangularis (BA 45).
---------------------------
TRACT
LABEL: cb_th_lr_tract in each subject dir goes from left cerebellum to right thalamus.
LABEL: cb_th_rl_tract in each subject dir goes from right cerebellum to left thalamus.
DESCRIPTION: a track that runs between a cerebellar hemisphere and the contralateral thalamus. Thought to be relevant for dyslexia.
DESCRIPTION OF STARTING ROI: The left (or right cerebellum) and the contralateral thalamus
ROIA: mni_Cb_l
ROIB: mni_thalamus_r
OR
ROIA: mni_Cb_r
ROIB: mni_thalamus_l
--------------------------
TRACT
LABEL: cst_l_tract and cst_r_tract
DESCRIPTION: Corticospinal tract. This tract from the brainstem to the motor-sensory area is clearly non-language and thus serves as a sort of comparison to other tracts (to determine whether all tracts are changing, or whether we are seeing differential change.)

ROIA: motor-sensory_l or motor-sensory_r
ROIB: brainstem_all
--------------------------
TRACT
LABEL: ec_l_tract and ec_r_tract
DESCRIPTION: extreme capsule (ec).  left and right.

ROIA:
LABEL: ec_spt_l and ec_spt_r
DESCRIPTION OF STARTING ROI: The temporal language area  subsumes five structures in
the left temporoparietal cortex: (1) angular gyrus, (BA39) “AG”; (2) anterior superior
temporal gyrus “ASTG”; (3) posterior middle temporal gyrus “PMTG”; (4) posterior
superior temporal gyrus “PSTG” and (5) supramarginal gyrus (BA40) “SMG”.
ROIB:
LABEL: ec_fr_l and ec_fr_r
DESCRIPTION OF STARTING ROI: The frontal language area subsumes three structures in the
left inferior frontal gyrus (IFG): the (1) pars opercularis (BA 44), (2) pars
triangularis (BA 45) and (3) pars orbitalis (BA 47).

--------------------------
TRACT
LABEL: ifg2ifg_tract
DESCRIPTION: callosal fibers from IFG_L to IFG_R.

ROIA:
LABEL: cc_ifg_l
DESCRIPTION OF STARTING ROI: The frontal language area subsumes three structures in the
left inferior frontal gyrus (IFG): the (1) pars opercularis (BA 44), (2) pars
triangularis (BA 45) and (3) pars orbitalis (BA 47).

ROIB:
LABEL: cc_ifg_r
DESCRIPTION OF STARTING ROI: The frontal language area subsumes three structures in the
left inferior frontal gyrus (IFG): the (1) pars opercularis (BA 44), (2) pars
triangularis (BA 45) and (3) pars orbitalis (BA 47).

---------------------------
TRACT
LABEL: ilf_l_tract and ilf_r_tract (inferior longitudinal fasciculus)
DESCRIPTION: (same as vwfatract)
ROIA:
LABEL: ilf_atl_l and ilf_atl_r (ilf, anterior temporal lobe parcellation)
DESCRIPTION OF STARTING ROI: entire atl, including temporal pole, stg_ant, mtg_ant,
itg_ant. All trimmed so they don't overlap.  Derived from Harvard-Oxford cortical atlas.
Same as used for mdlf and unc

ROIB:
LABEL: ilf_vwfa_l and ilf_vwfa_r (ilf, Visual Word Form Area parcellation)
DESCRIPTION OF STARTING ROI: 2 overlapping regions are binarized (no thresholding) and added
together to produce an oversized vwfa. ITG_tempocc is the Inferior Temporal
Gyrus-temporal occipital part from the Harvard Oxford Cortical atlas AND TOF
is the temporal occipital fusiform gyrus, posterior division from the same
atlas).  It is used for the callosal vwfa tract, and ilf.
--------------------------
TRACT
LABEL: iof_l_tract and iof_r_tract (inferior occipital fasciculus)
DESCRIPTION:
ROIA:
LABEL: iof_occ_l and iof_occ_r
DESCRIPTION OF STARTING ROI:   (Marsbar/aal library) whole occipital lobe
built up from the Marsbar aal library and containing the following:
occipital_inferior, occipital_mid and
occipital_superior + lingual, calcarine and cuneus.
This is a set of GM structures…at least initially we are not filling in the
obvious hunk of white matter as I’m hoping this will help dial back a little
on the sensitivity.  We’ll see how that works. (Used for iof tracking)
ROIB:
LABEL: iof_fr_l, iof_fr_r
DESCRIPTION OF STARTING ROI: frontal_orbital_l and r:
Marina: This includes all 5 orbital rois (used for
uncinate and iof tracking):
Frontal lobe/Orbital Surface/Superior Frontal Gyrus, orbital part Frontal
lobe/Orbital Surface/Superior Frontal Gyrus, medial orbital  Frontal
lobe/Orbital Surface/Middle Frontal Gyrus, orbital part  Frontal lobe/Orbital
Surface/Inferior Frontal Gyrus, orbital part Frontal lobe/Orbital Surface/Gyrus
Rectus (Note: uncinate2 includes brocas area 44 and 45, not just 47)
--------------------------
TRACT
LABEL: mdlf_l_tract and mdlf_r_tract
DESCRIPTION: (mdlf: middle longitudinal fasciculus)
ROIA:
LABEL: mdlf_atl_l and mdlf_atl_r (mdlf, anterior temporal lobe parcellation)
DESCRIPTION OF STARTING ROI: entire atl, including temporal pole, stg_ant, mtg_ant, itg_ant
All trimmed so they don't overlap.  Derived from Harvard-Oxford cortical atlas.
Same as used for ilf and unc.
ROIB:
LABEL:  mdlf_spt_l and mdlf_spt_r (mdlf, posterior temporal lobe parcellation)
DESCRIPTION OF STARTING ROI: The temporal language area  subsumes five structures in
the left temporoparietal cortex: (1) angular gyrus, (BA39) “AG”; (2) anterior superior
temporal gyrus “ASTG”; (3) posterior middle temporal gyrus “PMTG”; (4) posterior
superior temporal gyrus “PSTG” and (5) supramarginal gyrus (BA40) “SMG”.
(same as roi used for ec and arc EXCEPT it does not include the anterior portion of the
stg)
---------------------------
TRACT
LABEL: slf2_l_tract and slf2_r_tract
DESCRIPTION: (superior longitudinal fasciculus II)
ROIA:
LABEL: slf2_ag_l and slf2_ag_r
DESCRIPTION OF STARTING ROI: angular_r_diff_csf.angular_l and r
(from the Marsbar/aal library)

ROIB:
LABEL:slf2_mfg_l slf2_mfg_r
DESCRIPTION OF STARTING ROI: frontal_mid_l and r (Marsbar/aal library)
(note that BA-68 is the back half of this).  This is an endpoint for
tracking the slf2
--------------------------
TRACT
LABEL: spt2motor_l_tract and spt2motor_r_tract
DESCRIPTION: tract from spt area (wernickes-see ROIB below) to the motor area (BA6 and BA8).

ROIA:
LABEL: motor_x_spt_l and motor_x_spt_r
DESCRIPTION OF STARTING ROI: ba6and8_l and r
Following Frey 2008, defined BA6 and 8. (p 11437)
Specifically (in my case), in Marina, choose
Frontal lobe/Lateral surface/Superior frontal gyrus, dorsolateral-L
and Frontal lobe/Lateral surface/Middle frontal gyrus -L
Choose midpoint of combined roi: x=-24; y=34;z=38 and edit roi by removing
anterior/ventral 1/2 to unlimited depth, which is fairly easy to do in
Marina. Do equivalent on right. Converted to nifti, fslswapdim to switch L-R,
checked results, cluster_clean.shed 2000, checked results.

ROIB:
LABEL: spt_x_motor_l and spt_x_motor_r
DESCRIPTION OF STARTING ROI: The temporal language area  subsumes five structures in
the left temporoparietal cortex: (1) angular gyrus, (BA39) “AG”; (2) anterior superior
temporal gyrus “ASTG”; (3) posterior middle temporal gyrus “PMTG”; (4) posterior
superior temporal gyrus “PSTG” and (5) supramarginal gyrus (BA40) “SMG”.
Same as used for arc, ec, spt2spt

--------------------------
TRACT
LABEL: spt2spt_tract (interhemispheric tract)
DESCRIPTION: connection from one posterior (mostly temporal) language area to the other
across the corpus callosum.
ROIA:
LABEL: cc_spt_l
DESCRIPTION OF STARTING ROI: The temporal language area  subsumes five structures in
the left temporoparietal cortex: (1) angular gyrus, (BA39) “AG”; (2) anterior superior
temporal gyrus “ASTG”; (3) posterior middle temporal gyrus “PMTG”; (4) posterior
superior temporal gyrus “PSTG” and (5) supramarginal gyrus (BA40) “SMG”.
ROIB:
LABEL: cc_spt_r
DESCRIPTION OF STARTING ROI: The temporal language area  subsumes five structures in
the left temporoparietal cortex: (1) angular gyrus, (BA39) “AG”; (2) anterior superior
temporal gyrus “ASTG”; (3) posterior middle temporal gyrus “PMTG”; (4) posterior
superior temporal gyrus “PSTG” and (5) supramarginal gyrus (BA40) “SMG”.
--------------------------
TRACT
LABEL: unc_l_tract and unc_r_tract
DESCRIPTION: uncinate fasciculus (unc)
ROIA:
LABEL: unc_atl_l and unc_atl_r
DESCRIPTION OF STARTING ROI: entire atl, including temporal pole, stg_ant, mtg_ant, itg_ant
All trimmed so they don't overlap.  Derived from Harvard-Oxford cortical atlas.
Same as used for ilf and mdlf.
ROIB:
LABEL: unc_fr_l and unc_fr_l
(frontal_orbital_l and r):
Marina: This includes all 5 orbital rois (used for
uncinate and iof tracking):
Frontal lobe/Orbital Surface/Superior Frontal Gyrus, orbital part Frontal
lobe/Orbital Surface/Superior Frontal Gyrus, medial orbital  Frontal
lobe/Orbital Surface/Middle Frontal Gyrus, orbital part  Frontal lobe/Orbital
Surface/Inferior Frontal Gyrus, orbital part  Frontal lobe/Orbital Surface/Gyrus
Rectus This also includes BA 44 and 45, not just BA 47)

--------------------------
TRACT
LABEL: vof_l_tract and vof_r_tract
DESCRIPTION: (vertical occipital fasciculus: connection from angular gyrus to vwfa
as per Yeatman, J. D., Rauschecker, A. M., & Wandell, B. A. (2012). Anatomy of the
visual word form area: Adjacent cortical circuits and long-range white matter connections.
Brain and Language. doi:10.1016/j.bandl.2012.04.010
ROIA:
LABEL: vof_angular_l and vof_angular_r
DESCRIPTION OF STARTING ROI: angular gyrus

ROIB:
LABEL: vof_vwfa_l and vof_vwfa_l
DESCRIPTION OF STARTING ROI: 2 overlapping regions are binarized (no thresholding) and added
together to produce an oversized vwfa. ITG_tempocc is the Inferior Temporal
Gyrus-temporal occipital part from the Harvard Oxford Cortical atlas AND TOF
is the temporal occipital fusiform gyrus, posterior division from the same
atlas).  It is used for the callosal vwfa tract, and ilf.
--------------------------
TRACT
LABEL: vwfa2vwfa (interhemispheric tract)
DESCRIPTION: connection from one vwfa to the other across the corpus callosum.
ROIA:
LABEL: cc_vwfa_l
DESCRIPTION OF STARTING ROI: 2 overlapping regions are binarized (no thresholding) and added
together to produce an oversized vwfa. ITG_tempocc is the Inferior Temporal
Gyrus-temporal occipital part from the Harvard Oxford Cortical atlas AND TOF
is the temporal occipital fusiform gyrus, posterior division from the same
atlas).  It is used for the callosal vwfa tract, and ilf.

ROIB:
LABEL: cc_vwfa_r
DESCRIPTION OF STARTING ROI: 2 overlapping regions are binarized (no thresholding) and added
together to produce an oversized vwfa. ITG_tempocc is the Inferior Temporal
Gyrus-temporal occipital part from the Harvard Oxford Cortical atlas AND TOF
is the temporal occipital fusiform gyrus, posterior division from the same
atlas).  It is used for the callosal vwfa tract, and ilf.
================================================================
