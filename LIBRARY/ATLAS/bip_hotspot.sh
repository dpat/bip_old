#!/bin/bash


#paths
source img_profile.sh

: <<COMMENTBLOCK

###############################################################################
AUTHOR:         Dianne Patterson University of Arizona
DATE CREATED:   Updated 10/12/12. It is now 11:12 AM
----------------------------  DEPENDENCIES  -----------------------------------
The script is dependent on 'profiles' found in /usr/local/tools/REF/PROFILES.
(e.g., img_profile.sh, subject_profile.sh and project specific profiles). The
profiles define variable names. All project specific profiles source
subject_profile.sh which sources img_profile.sh.

Assumes we've run bip_stats.sh which creates the standard space images for each
subject in DTI/Bip_stand.
===============================================================================
----------------------------  PURPOSE  ----------------------------------------
This script will add image data to the ${D}/Bip_hotspot dir.  This entails
adding to an existing "all" image if it does not exist.  It must be run for each
project directory that will add to the hotspot data.

Get binary rois in Bip_stand
e.g., Get arc_l_A, arc_l_B, extcap_l_A, extcap_l_B
e.g, Get arc_r_A, arc_r_B, extcap_r_A, extcap_r_B
Add them together in Bip_hotspot as standard space images labeled with
an "all" prefix
===============================================================================
----------------------------  INPUT  ------------------------------------------
DTI/Bip_stand must exist in each subject dir and contain tract and endpoint
images in standard space.
===============================================================================
----------------------------  OUTPUT  -----------------------------------------
${D}/Bip_hotspot will have "all_*" images created for each endpoint and
tract. These can be added to until all relevant project directories have been
added.

###############################################################################

COMMENTBLOCK
###############################################################################
#########################  DEFINE FUNCTIONS  ##################################

: <<COMMENTBLOCK

Function:   GetProjectBipData
Purpose:    For the project (see SetupProject), get the standard space bip data
            or each subject and add it to the appropriate "all*" image.
Input:      /DTI/Bip_stand images
Output:     ${D}/Bip_hotspot/all* images have data added to them

COMMENTBLOCK

function GetProjectBipData
{


#Iteratively add each endpoint binary to the others to create the hotspot image
 for tract in `cat ${bip_list}`
   do
       for subject in `cat ${sublist_bip}`
       do
         if [ -e ${maindir}/${subject}/DTI/Bip_stand/${tract}_stand_wm_bin.nii.gz ]
         then
            fslmaths all_${tract}_stand -add ${maindir}/${subject}/DTI/Bip_stand/${tract}_stand_wm_bin all_${tract}_stand
            echo "Tract: ${maindir} ${subject} ${tract}_stand_wm"
            fslstats ${maindir}/${subject}/DTI/Bip_stand/${tract}_stand_wm_bin -V -R
         fi
         for AB in A B
         do
           if [ -e ${maindir}/${subject}/DTI/Bip_stand/${tract}_${AB}_stand_gm_bin.nii.gz ]
           then
               fslmaths all_${tract}_${AB}_stand -add ${maindir}/${subject}/DTI/Bip_stand/${tract}_${AB}_stand_gm_bin all_${tract}_${AB}_stand
               echo "Endpoints: ${maindir} ${subject} ${tract}_${AB}_stand_gm"
               fslstats ${maindir}/${subject}/DTI/Bip_stand/${tract}_${AB}_stand_gm_bin -V -R
           fi
         done
      done
   done

}

#==============================================================================
: <<COMMENTBLOCK

Function:   SetupOnce
Purpose:    Create ${D}/Bip_hotspot if it does not exist.
            Create all empty template images
Input:      Standard space file must exist to use as template
Output:     ${D}/Bip_hotspot and empty images with appropriate names

COMMENTBLOCK

function SetupOnce
{

D=/Volumes/Main/Exps/Data
# Set variable names
HS=${D}/Bip_hotspot
bip_list=${LISTS}/lst_bip.txt
echo ${HS}

# Create ${D}/Bip_hotspot if it does not exist
if [ -d ${HS} ]; then
    cd ${HS}
else
    mkdir ${HS}
    cd ${HS}
fi

# Create template for all standard images
if [ ! -e mask_stand.nii.gz ]
then
   fmask.sh ${STANDARD}/avg152T1_brain mask_stand
fi

# Create empty standard space images with the correct names,
# if they do not already exist.

 for tract in `cat ${bip_list}`
   do

      if [ ! -e  all_${tract}_A_stand ]; then
          imcp mask_stand all_${tract}_A_stand
      fi

      if [ ! -e  all_${tract}_A_stand ]; then
          imcp mask_stand all_${tract}_B_stand
      fi

      if [ ! -e  all_${tract}_A_stand ]; then
          imcp mask_stand all_${tract}_stand
      fi
   done
   echo "SetupOnce is done"

}

#==============================================================================
: <<COMMENTBLOCK

Function:   Main
Purpose:    Create hotspot images
Input:     	DTI/Bip_stand images for each subject
            Does dtierp and then plante
Output:     ${D}/Bip_hotspot "all*" images

COMMENTBLOCK

function Main
{
    SetupOnce
    # SetProfile Dystract
    # GetProjectBipData
    # maindir=${DYSTRACT}
    # echo "done with dystract"


    # SetProfile dtierp 				# This profile sets maindir to dtierp
    # GetProjectBipData
    # echo "done with dtierp"
    # SetProfile plante
    # maindir=${WB1}
    # GetProjectBipData
    # echo "done with wb1"


}

#########################  END FUNCTION DEFINITIONS  ##########################
###############################################################################
#paths
source img_profile.sh
# Load shared functions
. functions.sh

# Note 3/11/2014: I've move SetProfile to functions.sh.
# This should still work but is untested

Main

# fslview avg152T1_brain.nii.gz all_arc_l_A_stand -l Blue-Lightblue all_arc_l_B_stand
# -l Blue-Lightblue all_extcap_l_B_stand -l Red-Yellow  all_extcap_l_A_stand -l Red-Yellow


MyLog                                           # Log script, args & date
