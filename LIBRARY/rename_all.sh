#!/bin/bash

source ${PROJECT_PROFILE}

if [ $# -lt 3 ]
then
echo "rename files by substituting part of a string with another part"
echo "$0 oldpart newpart files"
echo "example: $0 _20 _50 \"Run0/wm*pos*\""
echo "this replaces all instances of _20 with _50 for files that meet the specification"
echo "regular expressions can be used to specify file names if the name is in quotes."
echo "example $0 _struct _T1w "anat/${SUBJECT}_struct*"      "
echo "In this case, we substitute nothing for an existing _20 in each filename"
     exit 1
fi

oldpart=$1
newpart=$2
oldfile=$3

for name in ${oldfile}; do
  newname=`echo ${name}| sed -e 's/'${oldpart}'/'${newpart}'/'`
  echo "${newname}"
  mv ${name} ${newname}
done

MyLog
