#!/bin/bash

# Written by Dianne Patterson, University of Arizona
# This is a general purpose script to create statistics for all scalar measurement images crossed with a given mask.
# The mask is specified as an argument. The output is a text file (stats) containing only the values.
# Probably you'll want to copy this text file to something with a better name since stats gets overwritten every time.

#paths
source ${PROJECT_PROFILE}

if [ $# -lt 1 ]
then
    echo "Usage: $0 <mask>"
    echo "Example: $0 DWI_DERIV/cst_l_constrained/fdt_bin_5p"
    echo "This is a general purpose script for generating mean values for scalar images:"
    echo "FA MD RD L1 (i.e., PD) and MO values for a mask given as an argument."
    echo "It assumes the scalar images can be uniquely identified by the initials (FA, MD etc)"
    echo ""
    exit 1
fi

mask=$1

vol=`fslstats ${mask} -V | awk '{printf $2 "\n"}'` #just get volume in cubic mm
mdiff=`fslstats ${DWI_DERIV}/ecc*MD.nii.gz -k ${mask} -M`
fa=`fslstats ${DWI_DERIV}/ecc*FA.nii.gz -k ${mask} -M`
rd=`fslstats ${DWI_DERIV}/ecc*RD.nii.gz -k ${mask} -M`
pd=`fslstats ${DWI_DERIV}/ecc*L1.nii.gz -k ${mask} -M`
mo=`fslstats ${DWI_DERIV}/ecc*MO.nii.gz -k ${mask} -M`

RmIf stats.txt

echo "${vol} ${mdiff} ${fa} ${rd} ${pd} ${mo}" >> stats.txt

MyLog
