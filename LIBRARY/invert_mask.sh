#!/bin/bash

# Create the inverse mask image from the specified mask image.
# appends _inverse to the output filename

#paths
source ${PROJECT_PROFILE}

if [ $# -lt 1 ]
then
    echo "Usage: $0 <mask_input_file>"
    echo "Example: $0  flair_csf"
    echo "output name=input_inverse"
    exit 1
fi

in=$1
input=`basename -s .nii.gz $1`

fslmaths ${input} -bin ${input} # Make sure the mask is binary
fslmaths ${input} -mul -1 -add 1 ${input}_inverse -odt input # Create the inverse mask (Mark Jenkinson's approach.  Very clever)
fslmaths ${input}_inverse -bin ${input}_inverse -odt input # Make sure the inverse mask is binary

MyLog
