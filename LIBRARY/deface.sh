#!/bin/bash

#paths
source ${PROJECT_PROFILE}


if [ $# -lt 1 ]
then
    echo "Usage: $0 <input file>"
    echo "Example: $0 mprage.nii.gz"
    echo "defaces a structural image, creating img_defaced.nii.gz"
    echo "prefers T1 images"
    exit 1
fi

input=$1
# Insure orientation is correct, as this helps deface.
echo "reorienteing"
fslreorient2std ${input} ${input}
# if .nii or .nii.gz appears in the name, the first call is invoked
# only if .nii.gz appears in the name is the second call invoked.
stem1=`basename -s .nii ${input}`
stem1=`basename -s .nii.gz ${input}`

echo "defacing"
if [ ! -e ${stem1}_defaced.nii.gz ]; then
  mri_deface ${input} talairach_mixed_with_skull.gca face.gca ${stem1}_defaced.nii

  gzip ${stem1}_defaced.nii
fi

# Generate a difference mask image. This can be easily used to compare the differences
# between input and output (and avoids all the freckles that occur in both).
# In the event that the defacing includes some brain, the deface_diff_mask can be edited
# and then used to mask the original again. -odt input should insure the result is not
# set to 32 bit if the original was not 32 bit.
if [ ! -e ${stem1}_deface_mask.nii.gz ]; then
  fslmaths ${stem1}_defaced -bin ${stem1}_defaced_mask
fi
echo "=================================================================="
echo "You should check your defacing results:"
echo ">fsleyes ${stem1} ${stem1}_deface_mask -cm Red"
echo "If the mask does not entirely overlap the brain, manually add voxels, then,"
echo ">fslmaths ${stem1} -mul ${stem1}_deface_mask ${stem1}_defaced"
