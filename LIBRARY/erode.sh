#!/bin/bash

#paths
source ${PROJECT_PROFILE}


if [ $# -lt 2 ]
then
    echo "Usage: $0 <input file> <# of reps> <dimensions>"
    echo "Example: $0 flair_mask 3 2D"
    echo "2D erodes a mask using 2D and 3x3x1 for the specified # of reps"
    echo "3D erodes a mask using 3D and 3x3x3 for the specified # of reps"
    echo ""
    exit 1
fi

input=$1
reps=$2

if [ $# -eq 2 ]
then
  dimensions="2D"
  else
  dimensions=$3
fi

i=1
while [ $i -le $reps ]
do
	echo "eroding ${i}"
	fslmaths $input -kernel ${dimensions} -ero $input -odt input
	let i+=1
done

MyLog
