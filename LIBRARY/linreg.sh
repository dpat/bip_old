#!/bin/bash

#Written by Dianne Patterson, University of Arizona
# Needs revision to handle i flag
#paths
source ${PROJECT_PROFILE}

###############################################################################
#########################  DEFINE FUNCTIONS  ##################################

: <<COMMENTBLOCK

Function:   HelpMessage
Purpose:    Display help
Calls:      None

COMMENTBLOCK

HelpMessage ()
{
echo
echo "Usage: $0 [-i|-m|-o] <infile> <ref_img> <matrix> <output_name>"
echo "E.g.: $0 -i SubjectROIs/milf_ant_l_diff_csf struct Reg/diff2str.mat silly_str"
echo
echo "This script runs flirt reslicing"
echo "Specify an option flag (-i=image; -m=mask; -o=outline)"
echo "1) Specify path to the file to be altered"
echo "2) Specify reference image (space your file should end up in, e.g. flair_brain)"
echo "3) Specify path to relevant mat file (matrix file, *.mat or *.xfms)"
echo "4) The output image will have the output_name"
echo
echo "NOTE"
echo "-m removes image; -o removes image and mask"
echo "This strategy is to avoid clutter"
echo "To make all 3, use -o first, then -m, then -i"
exit 1
}

#==============================================================================
: <<COMMENTBLOCK

Function:   MkMask
Purpose:    Make a binary thresholded mask file resliced into a different space
Input:      A binary mask
Output:     A binary mask in the new space, labelled *_bin.nii.gz
Calls:      Reslice from this script

COMMENTBLOCK

MkMask ()
{
Reslice ${input} ${ref} ${mat} ${output}
imcp ${output} ${output}_bin
fslmaths ${output}_bin -thr 0.5 -bin ${output}_bin
}

#==============================================================================
: <<COMMENTBLOCK

Function:   Reslice
Purpose:    Run flirt to create image in new space
Input:      input=Image to be resliced,
            ref=a reference image (we will reslice INTO this new space)
            -init mat= a mat (matrix) file that expresses the relationship between
            the input space and the output space
Output:     Image in new space
Calls:      None

COMMENTBLOCK

Reslice ()
{
flirt -in ${input} -datatype float -ref ${ref} -init ${mat} \
-applyxfm -out ${output}
}

#########################  END FUNCTION DEFINITIONS  ##########################
###############################################################################

if [ $# -lt 1 -o $# -gt 1 -a $# -lt 5 ]
then
    HelpMessage
fi
                                                # Option flags
for arg in $*; do                               # For all commandline args
  case "$1" in                                  # Get the first arg
    -h|-help) HelpMessage;;                     # If arg=-h or -help, display help message
    -i) image="yes";                            # Set image=yes
        shift;;                                 # Shift to next arg
    -m) mask="yes";                             # Set mask=yes
        shift;;                                 # Shift to next arg
    -o) outline="yes";                          # Set outline=yes
        shift;;                                 # Shift to next arg
    -im|-mi) im="yes";                          # Set im=yes
        shift;;                                 # Shift to next arg
    -mo|-om) mo="yes";                          # Set mo=yes
        shift;;                                 # Shift to next arg
    -io|-oi) io="yes";                          # Set io=yes
        shift;;                                 # Shift to next arg
    -imo|-iom|-moi|-mio|-oim|-oim) imo="yes";   # Set imo=yes
        shift;;                                 # Shift to next arg
  esac                                          # End case
done                                            # End for loop

input=$1                                        # Define input
ref=$2                                          # Define reference image
mat=$3                                          # Define matrix
output=$4                                       # Define output image

if [ "${image}" = "yes" ]; then                 # If image flag is yes, then reslice
  Reslice ${input} ${ref} ${mat} ${output}
fi

if [ "${mask}" = "yes" ]; then                  # Make mask only
  MkMask ${output}                              # Make the mask
  imrm ${output}                                # rm image file
fi

if [ "${outline}" = "yes" ]; then               # Make outline only
  MkOutline ${output}                           # Make outline
  imrm ${output}                                # rm image file
  imrm ${output}_bin                            # rm mask
fi

if [ "${im}" = "yes" ]; then                    # Make image & mask
  MkMask ${output}
fi

if [ "${mo}" = "yes" ]; then                    # Make mask & outline
  MkOutline ${output}
  imrm ${output}
fi

if [ "${io}" = "yes" ]; then                    # Make image & outline
  MkOutline ${output}
  imrm ${output}_bin
fi

if [ "${imo}" = "yes" ]; then                   # Make image, mask & outline
   MkOutline ${output}
fi

MyLog                                           # Log date, script, options in log.txt
