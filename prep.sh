#!/bin/bash
#.bip_profile takes an argument that sets the project specific profile.
# This way we can source the project specific profile with a variable name.

source ${PROJECT_PROFILE}
#==============================================================================

: <<COMMENTBLOCK

Function:   HelpMessage
Purpose:    Display help
Calls:      None

COMMENTBLOCK

HelpMessage ()
{
echo ""
echo "Set up your profile, e.g., bip_min_profile.sh"
echo "the profile indicates the presence of fieldmaps, reverse phase encode images or lesion masks"
echo "the profile also supplies the expected filenames, BIDS style names used by default"
echo ""
exit 1
}

#==============================================================================

: <<COMMENTBLOCK

Function:   Main
Purpose:    Run main functions: dtifit, bedpost, eddy
Input:      A directory created by setup.sh
COMMENTBLOCK

Main ()
{
if [ ! -e ${DWI_DERIV}/${SUBJECT}_prep_started ]; then
  touch ${DWI_DERIV}/${SUBJECT}_prep_started
  # Run motion and distoriton correction on dwi image
  DWICorrect
  # epi_reg to do boundary based registration of DWI to structural
  BBRDWI
  # dtifit and bedpostx
  MkBed
elif [ -e ${DWI_DERIV}/${SUBJECT}_prep_started ]; then
  	echo "already started prep.sh for this subject, moving on"
fi
}

#==============================================================================
: <<COMMENTBLOCK

COMMENTBLOCK

for arg in $*; do                               # For all commandline args
  case "$1" in                                  # Get the first arg
    -h|-help) HelpMessage;;                     # If arg=-h or -help, display help message
  esac                                          # End case
done                                            # End for loop

echo "Fieldmap? ${FM_exist}"
echo "Topup? ${TOP_exist}"
echo "Lesion? ${LESION_exist}"
Main

#==============================================================================
