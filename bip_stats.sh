#!/bin/bash
#.bip_profile takes an argument that sets the project specific profile.
# This way we can source the project specific profile with a variable name.

source ${PROJECT_PROFILE}
#==============================================================================

# bip_stats.sh automates getting text data into the correct format to
# import into access for my bip runs.
# It calls bip_stats_sub.sh which calls FindMax and getstats_dti.sh

#paths

Func=$1 # Main or Main2 or CleanAll

if [ $# -lt 1 ]
then
    echo "Usage: $0 <FunctionName> "
    echo "Example: $0 Main"
    echo "You need to enter the function to be run. e.g., Main, CleanAll"
    echo "bip_stats calls bip_stats_sub for 6 of 18 possible tracts of interest"
    exit 1
fi

bip_stats_sub.sh ${Func} arc5 l                         # dorsal language tract
bip_stats_sub.sh ${Func} arc5 r                        # dorsal language tract homologue
bip_stats_sub.sh ${Func} aslant l        	            # brocas to Supplementary Motor Area
bip_stats_sub.sh ${Func} aslant r       	            # brocas to SMA
bip_stats_sub.sh ${Func} cb_th_lr ih                  # cb to opposite thalamus
bip_stats_sub.sh ${Func} cb_th_rl ih                  # cb to opposite thalamus
#bip_stats_sub.sh ${Func} extcap5 l                      # ventral language tract
#bip_stats_sub.sh ${Func} extcap5 r                      # ventral language tract homologue
bip_stats_sub.sh ${Func} ilf3 l                         # atl to vwfa (inferior longitudinal fasciculus)
bip_stats_sub.sh ${Func} ilf3 r                         # atl to vwfa (inferior longitudinal fasciculus)
bip_stats_sub.sh ${Func} iof l                          # frontal to occipital: inferior occipital fasciculus
bip_stats_sub.sh ${Func} iof r                          # frontal to occipital: inferior occipital fasciculus
# bip_stats_sub.sh ${Func} mdlf l                         # ant temporal (atl) to wernickes5
# bip_stats_sub.sh ${Func} mdlf r                         # ant temporal (atl) to wernickes5
bip_stats_sub.sh ${Func} slf2 l                         # AG to mid frontal
bip_stats_sub.sh ${Func} slf2 r                         # AG to mid frontal
bip_stats_sub.sh ${Func} unc2 l                         # uncinate brocas++ to atl
bip_stats_sub.sh ${Func} unc2 r                         # uncinate brocas++ to atl
bip_stats_sub.sh ${Func} b3tob3 ih                      # callosal connections: brocas to brocas
# bip_stats_sub.sh ${Func} vwfa2vwfa ih                   # callosal connections: vwfa to vwfa
bip_stats_sub.sh ${Func} w5tow5 ih                      # callosal connections: wernickes to wernickes
bip_stats_sub.sh ${Func} vof l                          # connections from vwfa to angular
bip_stats_sub.sh ${Func} vof r                          # connections from vwfa to angular
# bip_stats_sub.sh ${Func} wernickes_motor l              # connections from wernickes to the motor region
# bip_stats_sub.sh ${Func} wernickes_motor r              # connections from wernickes to the motor region

MyLog
