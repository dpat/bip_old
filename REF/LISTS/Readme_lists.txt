
This directory contains lists of 2 sorts.  All are named lst*.txt:
Lists of subjects:

 (lst_subj_*.txt) for particular projects, or within project by some criterion.
========================
Lists of regions:

standard images grouped by some criterion,
lst_tract_seed_target_masks.txt is a list of the target pairs to be used in
probtrack2

lst_bip.txt a list of tracts defined and available to be run

lst_group_rois.txt is a list of lobe groups, hemispheres etc.

lst_build_rois.txt is a list of standard space rois that are used to build tracking rois.
For example, BA44, 45 and 47 will be converted to dti space and added together to make a mask for
brocas.  This is used in tracking the arcuate and extreme capsule.
==========================
