Created 2013/06/29
Updates 2017/08/03

Siemens default phase encode direction is A-P.  This should correspond to a
negative phase encode direction (blip-down), the value "-1" and elongated
(stretchy) eyeballs.  We will collect diffusion weighted images in this direction.

The reverse phase-encode direction is P-A. This is a positive phase-encode,
(blip-up), the value "1" and squishy eyeballs, temporal lobes and frontal lobes.

In my assessment I compared 8 dti sets corrected with topup and eddy.  The results
of correcting the default phase encode (A-P) images had fewer jarring artifacts (less noise, fewer spikes and holes in weird places, better eyeball reconstruction).

The following files can be found in this directory:

The acqp files are used by eddy, as described here:
http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/EDDY/UsersGuide
This is a text-file describing the acquisition parameters for the different images
in --imain. The format of this file is identical to that used by topup (though the
parameter is called --datain there).
The value is repeated 32 times for the 32 volumes.  The -1 indicated the phase
encode direction for y in A-P.  1 is for phase encode direction for P-A. The
last value is the total readout time as calculated by Scott Squire and I and
confirmed with FSL listserv.

0 -1 0 0.0602 (for A-P)

acqp_A-P.txt
===================
acqparams.txt: Used for topup.  Specifies the values for the 4 blip_down and
                        blip_up B0 volumes

index_A-P.txt: Used by eddy to index all the values in acqp_A-P.txt (all 1's)
====================
