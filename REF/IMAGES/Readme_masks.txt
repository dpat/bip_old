A RECORD OF HOW VARIOUS ROIS WERE DERIVED.
Rois came from (Marina, Marsbar, Harvard-Oxford, MNI).
They have been altered (manually edited, sliced at COG, dilated and
eroded...).

For rois that already exist in standard space, the following can be done:
using the original roi or some carefully designed transform of it), we do a 12 DOF
(degrees of freedom) affine rotation, translation and global scaling with
flirt of the standard to the structural (spgr) space or dti space.  We threshold
the masks at 0.5 as recommended in the FSL FAQ (
http://www.fmrib.ox.ac.uk/fslfaq/#flirt_mask ) and check to make sure that
flirting did not result in vastly altered mask size:  “a threshold of 0.5
will (approximately) preserve the size of the original mask any voxel in the new
space that overlaps by 50% or more with the original mask will be included
in the new binary mask”.
Then, we view the image and make sure it lines up with expectations…rules
for nudging or editing each structure will have to be developed.
What’s good: The technique starts from a known accepted average.
The technique automatically attempts to match the mask to the individual
brain. Manual editing of such a structure results in measurable differences from
the automated one…such differences could be data mined later for interesting
properties.
We never normalize/warp our structural images to the standard space, but we
still get a standard to compare to because we have the average and we can
track the manipulations to that average.
============================================
SUBDIRECTORY
Inverse: These are masks for each tract which carefully define a small region
where tracking is allowed
============================================
angular_l and r (Marsbar/aal library)
    Used as endpoint for slf2 and included
    in arcuate/extcap endpoint (used to be mni_angular)
============================================
atl_l and r (anterior temporal lobe)
    Today is 2/8/13. It is now 2:48 PM

   I used the aal atlas in the wfu-pickatlas to extract the following 6 rois for each side:
   fusG
   itg
   mtg
   pHg
   stg (includes transverse temporal gyrus in aal atlas)
   tp (combined superior and mid temporal pole)
   ======================
   Using my existing temporal_lobe masks and my hemisphere_post masks I subtracted
   the post from the temporal lobe (left and right) to produce an initial mask
   covering the anterior half of each temporal lobe. This is called the
   atl_cutoff-mask and will be further manually edited to match the Binney 2010
   criterion. This includes making sure the cutoff mask is perpendicular to the
   long axis of the temporal lobe. (manually in imango) and defining the cutoff
   point as y=-26 on the inferior edge (this is the peak of the curve along the
   inferior lateral edge of the temporal lobe).

   GALTON
   Following the definitions of temporal subvolumes measured by Galton et al., each
   ROI was trimmed such that their posterior boundaries lay at the coronal slice at
   which the Sylvian aqueduct is first visible (MNI y = –26; slice identified using
   SPM5’s avg152T1 image). 5 rois: stg (includes transverse) , mtg, itg, fusG, pHg
   Galton (2001)

   Temporal pole—The anterior temporal structures (white and gray
   matter) were manually traced on all slices where distinguishable until the slice
   prior to closure of the lateral fissure. This encompassed predominantly temporal
   pole (Brodmann area [ba] 38) but may include small sections of entorhinal
   cortex, fusiform, inferior, and middle temporal gyri, and superior temporal gyri
   (ba 28, 36, 20, 21, and 22). (for us, this is Y=66 on the avg152T1).

   Parahippocampal gyrus—The anatomic correlates of
   this measure are thought to include most of the entorhinal cortex (ba 28) and
   some of the perirhinal cortex in the wall of the collateral sulcus (ba 35).

   Fusiform gyrus—The neuroanatomic correlates of this measure were thought to be
   ba 36 and a small part of ba 35.

   Inferior and middle temporal gyri—Owing to
   variability across subjects these areas were taken together. The medial border
   was the line from the reference point to the inferior temporal sulcus and the
   superolateral border was the inferomedial border of the superior temporal gyrus
   measure. The assumed anatomic correlates for this measure were ba 20 and 21.

   Superior temporal gyrus—The anatomic correlates of this measure are thought to
   be the superior temporal gyrus and transverse temporal gyrus, ba 22 with some 41
   and 42.

   Binney 2010 indicate that they used the wfu_pickatlas, but they do not indicate which
   atlas(es) they actually used. The aal looks pretty good.

    ==============================================================================
    12/21/2012 created better version of roi: From Harvard Oxford Cortical atlas:
    the temporal pole + the anterior STG, anterior MTG and anterior ITG. Each of
    these was thresholded (-thrP ...threshold nonzero voxels) at 25%.
    Trimmed to keep tp (trim the 3 others). Preferentially trim overlap from the
    mtg_ant
    fslmaths itg_ant -sub tp -bin itg_ant
    fslmaths mtg_ant -sub tp -bin mtg_ant
    fslmaths stg_ant -sub tp -bin stg_ant
    fslmaths mtg_ant -sub itg_ant -sub stg_ant -bin mtg_ant

    and added together an binarized. This approach results in a better image,
    easier to reproduce.

    The atl is thus constructed from 4 nonoverlapping components: tp, itg_ant,
    mtg_ant and stg_ant.

    Originally a hand modified roi that covers the temporal pole
    and more but not the entire front half of the temporal lobe. It is used for
    tracking mlf, ilf, milf, unc2, and ilf3
==============================================================================
ba6and8_l and r
    Following Frey 2008, defined ba6 and 8. (p 11437)
    Specifically (in my case), in Marina, choose
    Frontal lobe/Lateral surface/Superior frontal gyrus, dorsolateral-L
    and Frontal lobe/Lateral surface/Middle frontal gyrus -L
    Choose midpoint of combined roi: x=-24; y=34;z=38 and edit roi by removing
    anterior/ventral 1/2 to unlimited depth, which is fairly easy to do in
    Marina.
    Do equivalent on right. Converted to nifti, fslswapdim to switch L-R,
    checked results, cluster_clean.shed 2000, checked results.
==============================================================================
ba44_l and r (split this into a dorsal and ventral parts) From Marina Frontal
lobe/Lateral surface/Inferior frontal gyrus, opercular part
==============================================================================
ba45_l and r From Marina Frontal lobe/Lateral surface/Inferior frontal gyrus,
triangular part
==============================================================================
ba47_l and r From Marina Frontal lobe/Orbital surface/Inferior frontal gyrus,
orbital part
==============================================================================
brocas_l and r
    For each subject:
    Brocas: Includes 2 components of the inferior frontal gyrus: the triangular
    (ba45) and the opercular (ba44).
    (Used for arcuate tracking; and once for slf3 tracking) (from Marina)
  ==============================================================================
brocas2 4/16/10  brocas2_l and brocas2_r include the following from Marina:
    Frontal lobe/Lateral surface/Inferior frontal gyrus, opercular part (i.e.,
    ba 44); Frontal lobe/Lateral surface/Inferior frontal gyrus, triangular part (i.e.,
    ba 45); Frontal lobe/Orbital surface/Inferior frontal gyrus, orbital part (i.e., ba
    47);

    brocas plus brocas2 = brocas3
    brocas3: It turns out that just because brocas2 is a superset of brocas in
    standard space does not always mean it is a superset in subject space
    (though this seems very weird).  I have added Brocas and Brocas2 in each subject
    space to create brocas3.  I used the script ftrim_extcap_arc3.
    temporal_sup_post_r_diff_csf plus  wernickes =  wernickes3

    inverse masks arc and extcap4:
    imcp arc2_l_inv_mask arc4_l_inv_mask
    imcp arc2_r_inv_mask arc4_r_inv_mask
    imcp extcap_l_inv_mask extcap4_l_inv_mask
    imcp extcap_r_inv_mask extcap4_r_inv_mask

    fslmaths arc4_l_inv_mask -sub mni_angular_l -bin arc4_l_inv_mask
    fslmaths arc4_r_inv_mask -sub mni_angular_r -bin arc4_r_inv_mask
    fslmaths extcap4_l_inv_mask -sub mni_angular_l -bin extcap4_l_inv_mask
    fslmaths extcap4_r_inv_mask -sub mni_angular_r -bin extcap4_r_inv_mask

    Manual editing for each one:
    fv /usr/local/fsl/data/standard/MNI152_T1_2mm_brain arc4_l_inv_mask -l Red

    ftrim_extcap_arc4 puts the correct mask in subject space and subtracts
    subject wernickes4 from it JUST to make sure.

    brocas_classic=ba44+ba45
==============================================================================
mni_Cb_l and mni_Cb_r: took whole Cerebellum from MNI atlas, used Left
	and right rectangles to separate the left and right. These were eroded
	once and then fairly aggressively modified near the midline brainstem
	and 4th ventricle.  Their purpose is for tracking from the thalamus to
	Cb (contralateral) as part of the reading network affecting dyslexia. It
	was more important to prevent tracking down the brainstem ventricle than
	preserving near midline grey matter of the cerebellum.
==============================================================================
cb_th_lr_inv_mask (left Cb to right thalamus).
cb_th_rl_inv_mask  (right Cb to left thalamus)
==============================================================================
cing_gyrus_20_l and r
    Used for IJN project: These come from the Harvard Oxford cortical atlas is
    FSL.  They come as an ant
    and posterior cingulate that is not L-R divided.
    They are probabalistic 1-100. I added these together, then multiplied by
    hemisphere_l to get the left side and hemisphere_r to get the right side.
    They are huge unthresholded, but thresholding at 50 makes them too small.  So,
    I'm thresholding at 20.

cingulum_ant_l and r, cingulum_mid_l and r, cingulum_post_l and r (Marsbar/aal
   library)
==============================================================================
csf
    a standard space csf mask (mutually exclusive of gm and wm)
==============================================================================
frontal_inf_orb_l and r (Marsbar/aal library)
==============================================================================
frontal_mid_l and r (Marsbar/aal library)
    (note that ba-68 is the back half
    of this).  This is an endpoint for tracking the slf2
==============================================================================
frontal_orbital_l and r:
    Marina: This includes all 5 orbital rois (used for
    uncinate and iof tracking):
    Frontal lobe/Orbital Surface/Superior Frontal Gyrus, orbital part Frontal
    lobe/Orbital Surface/Superior Frontal Gyrus, medial orbital  Frontal
    lobe/Orbital Surface/Middle Frontal Gyrus, orbital part  Frontal
    lobe/Orbital Surface/Inferior Frontal Gyrus, orbital part Frontal lobe/Orbital
    Surface/Gyrus Rectus (Note: uncinate2 includes brocas area 44 and 45, not just 47)
==============================================================================
frontal_sup_l and r (Marsbar/aal library)
==============================================================================
Fr_MNI_l (and r)
Frontal lobe for thalamus tracking.  Comes directly from MNI atlas in FSL, masked to remove opposite hemisphere.
==============================================================================
RightMask2mm and LeftMask2mm (left and right hemispheres (rectangles)
==============================================================================
ilf_ant_l and r and post_l and r
    First attempt to define rois for tracking the ilf (from anterior to
    posterior)...lots of manual work and fip results

ilf2_ant_l and r
    Second attempt to define rois for tracking the ilf (from anterior to
    posterior)...lots of manual work and fip results

itg_ant_l and itg_ant_r
    From Harvard Oxford Cortical atlas, thresholded (-thrP ...threshold nonzero
    voxels) at 25%. One component of the revised atl (12/21/2012)

mdlf_post_l and mdlf_post_r
    Similar to wernickes5, but does not use the anterior portion of the stg.
    See bip_prep.sh for the build function.

mtg_ant_l and mtg_ant_r
    From Harvard Oxford Cortical atlas, thresholded (-thrP ...threshold nonzero
    voxels) at 25%. One component of the revised atl (12/21/2012)

mtg_l and r: Marina (lateral middle temporal gyrus) (used to be mtl,
    changed to mtg on 2/1/13, 3:23 PM (mtg_l, mtg_r, mtg_post_l, mtg_post_r)

occipital_l (and r):
    (Marsbar/aal library) whole occipital lobe built up from the Marsbar aal
    library and containing the following occipital_inferior, occipital_mid and
    occipital_superior + lingual, calcarine and cuneus.
    This is a set of GM structures…at least initially we are not filling in the
    obvious hunk of white matter as I’m hoping this will help dial back a little
    on the sensitivity.  We’ll see how that works. (Used for iof tracking)

Par_MNI_l (and r)
Parietal lobe for thalamus tracking.  Comes directly from MNI atlas in FSL, masked to remove opposite hemisphere.

parietal_sup_l and r (Marsbar/aal library)

precuneus_sup_l and r
    (Marsbar/aal library) precuneus roi, this was halved (inferior/superior)
    using the marsbar center of gravity.  Only the superior half was used as an
    roi for slf1.


sma_l and sma_r (supplementary motor area)
   Following Catani 2012 on the aslant tract as closely as possible:
   # Add ant and post cing:
   >fslmaths ant_cing.nii.gz -add pos_cing.nii.gz cing.nii.gz
   # Make binary, (note, no 50% threshold…I liked the whole extent for this task):
   >fslmaths cing -bin cing_bin
   # Subtract cing from sma_lr after making sma_lr binary:
   (sma_lr comes from the Harvard-Oxford Cortical atlas, where it is now called the
   Juxtapositional Lobule Cortex (formerly Supplementary Motor Cortex))
   >fslmaths sma_lr -bin sma_lr_bin
   >fslmaths sma_lr_bin -sub cing_bin -bin sma_lr_stage1
   Also, simply remove anything left at or below Z=64 vox (56 mm):
   sma_lr_stage2
   This only picks out the most medial gyrus of the SGF of interest though…
   My ROI is only frontal, not parietal (that's good)…so it does NOT go too far posterior.
   All indications are that I should leave the A-P extent as it is:
   Y 72 (18 mm) anterior to Y=51 (-24 mm) posterior
   And manually add the 2nd gyrus. Call this sma_lr_stage3.
   The bi-lobar (2 gyrus) structure disappears posterior to Y=60 (-6mm),
   so I don't continue the addition of the 2nd gyrus after Y=60
   >fslmaths sma_lr_stage3 -mul ../Lobe_Hemi_Brain/hemisphere_r -bin sma_r
   >fslmaths sma_lr_stage3 -mul ../Lobe_Hemi_Brain/hemisphere_l -bin sma_l


stg_ant_l and stg_ant_r
    From Harvard Oxford Cortical atlas, thresholded (-thrP ...threshold nonzero
    voxels) at 25%. One component of the revised atl (12/21/2012)

stg_post_l and stg_post_r
	same as temporal_sup_post

stg_l and stg_r
	temporal_sup_ant+temporal_sup_post

supramarginal_l and r (Marsbar/aal library)

temporal_ant_l and r
    (this gets subtracted from the MTL when creating
    wernickes5.  It is the front half of the temporal lobe.)

temporal_sup_post_l and r AAL library Temporal_sup gyrus, chopped in half at
    the center of gravity in Marsbar…only the posterior half kept as that is
    essentially Wernicke’s area (5). (Used for arcuate and emc tracking). See
    stg_post (different name, same thing)

temporal_pole_sup_l and r (Marsbar/aal library)

Tmp_MNI_l (and r)
Temporal lobe for thalamus tracking.  Comes directly from MNI atlas in FSL, masked to remove opposite hemisphere.

tp_l and tp_r (temporal pole left and right)
    From Harvard Oxford Cortical atlas, thresholded (-thrP ...threshold nonzero
    voxels) at 25%. One component of the revised atl (12/21/2012)

vwfa_l and r
    (2 overlapping regions are binarized (no thresholding) and added
    together to produce an oversized vwfa. ITG_tempocc is the Inferior Temporal
    Gyrus-temporal occipital part from the Harvard Oxford Cortical atlas AND TOF
    is the temporal occipital fusiform gyrus, posterior division from the same
    atlas).  It is used for the callosal tract, and ilf3

unc and unc2 _l and _r
	frontal_orbital_l and r:
    Marina: This includes all 5 orbital rois (used for
    uncinate and iof tracking):
    Frontal lobe/Orbital Surface/Superior Frontal Gyrus, orbital part Frontal
    lobe/Orbital Surface/Superior Frontal Gyrus, medial orbital  Frontal
    lobe/Orbital Surface/Middle Frontal Gyrus, orbital part  Frontal
    lobe/Orbital Surface/Inferior Frontal Gyrus, orbital part Frontal lobe/Orbital
    Surface/Gyrus Rectus (Note: uncinate2 includes brocas area 44 and 45, not just 47)

    atl_l and r (anterior temporal lobe)
    a hand modified roi that covers the temporal pole
    and more, but not the entire front half of the temporal lobe.
    It is used for tracking mlf, ilf, unc2

    Today is 6/19/12. It is now 2:18 PM
    In the Inverse subdir unc2 has replaced unc...but they were the same.

wernickes5_l and r:
  STG, SMG, AG and the post MTG.
  fslmaths MTL_l -sub temporal_ant_l MTL_post_l
  fslmaths MTL_r -sub temporal_ant_r MTL_post_r
