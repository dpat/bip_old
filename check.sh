#!/bin/bash
#.bip_profile takes an argument that sets the project specific profile.
# This way we can source the project specific profile with a variable name.

source ${PROJECT_PROFILE}

#==============================================================================

: <<COMMENTBLOCK

Function:   BET_Anat
Purpose:    Display anatomy file and BET mask for review
Input:      anat image and corresponding brain mask.
Calls:      CheckBET_Anat in bip_functions.sh
COMMENTBLOCK

BET_Anat ()
{
CheckBET_Anat
}
#==============================================================================

: <<COMMENTBLOCK

Function:   BET_B0
Purpose:    Display B0 file and BET mask for review
Input:      B0 image and the corresponding brain mask.
Calls:      CheckBET_B0 in bip_functions.sh
COMMENTBLOCK

BET_B0 ()
{
# Register segmentations to B0 space, make test registrations
CheckBET_B0
}

#==============================================================================

: <<COMMENTBLOCK

Function:   Deface
Purpose:    Check and review defacing (insure brain voxels were not removed)
Input:      anat image and corresponding difference mask showing
            which voxels were removed by defacing.
Calls:      CheckDeface in bip_functions.sh
COMMENTBLOCK

Deface ()
{
CheckDeface
}

#==============================================================================

: <<COMMENTBLOCK

Function:   Reg
Purpose:    Check and review registraions
Input:      A directory created by setup.sh
Calls:      CheckReg in bip_functions.sh
COMMENTBLOCK

Reg ()
{
# Register segmentations to B0 space, make test registrations
CheckReg
}

#==============================================================================

: <<COMMENTBLOCK

Function:   HelpMessage
Purpose:    Display help
Calls:      None

COMMENTBLOCK

HelpMessage ()
{
echo ""
echo "check.sh calls the selected viewer to display registrations, BET masks, and defacing."
echo "e.g. check.sh BET_Anat will display the BET mask on the anatomy file"
echo "e.g. check.sh BET_B0 will display the BET mask on the B0 file"
echo "e.g. check.sh Deface will display the anatomy file and a mask of voxels removed by defacing"
echo "e.g. check.sh Reg will create (if needed) and display registrations:"
echo "B0toMNI, MNItoB0, anat segmentations to DWI"
exit 1
}

#==============================================================================
: <<COMMENTBLOCK

COMMENTBLOCK
#if [ $* $# -lt 1 ]; then

if [ $# -lt 1 ]; then
  HelpMessage
fi

for arg in $*; do                               # For all commandline args
  case "$1" in                                  # Get the first arg
    -h|-help) HelpMessage;;                     # If arg=-h or -help, display
    \?) echo "only h is a valid flag" 1>&2   		# If bad options are passed, print help
  esac                                          # End case
done                                            # End for loop

shift $(( $OPTIND -1))                          # Index option count, so h isn't treated as arg
optnum=$(( $OPTIND -1))                         # optnums hold # of options passed in

if [ ${optnum} -lt 1 -a $# -eq 1 ]; then        # If no options
  dothis=$1                                      # then first arg is function
fi
$dothis                                    			# Execute requested function

MyLog                                           # Log script, args & date

#==============================================================================
