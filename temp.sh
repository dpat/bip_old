#!/bin/bash
#.bip_profile takes an argument that sets the project specific profile.
# This way we can source the project specific profile with a variable name.

source ${PROJECT_PROFILE}
#==============================================================================

: <<COMMENTBLOCK
This is a test file

COMMENTBLOCK
Main ()
{
# mv Morph anat
# mv DWI dwi
# mv *_Archive archive
# mv archive ${SUBJECT}_archive
# mv Reg reg
# mv Regtest regtest
# mv Stats stats
# mv SubjectROIs subjectrois
# cd anat
# for f in *; do
# 	echo $f
#   mv $f sub-${f}
# done
#
# cd ../dwi
# for f in *; do
# 	echo $f
#   mv $f sub-${f}
# done
#
# immv ${SUBJECT}_dwi_A-P ${SUBJECT}_AP_dwi
# immv ${SUBJECT}_dwi_P-A ${SUBJECT}_PA_dwi
#
# cd ../reg
# for f in *; do
# 	echo $f
#   mv $f sub-${f}
# done
# cd ..
# mv dwi/sub-Bed dwi/Bed
# #
#SUBJECT=`echo "hello"`
#pwd
#echo "the subject is ${SUBJECT}"
#mv anat/${SUBJECT}_mprage.anat ${SUBJECT}_archive
#mv anat/${SUBJECT}_mprage.nii.gz ${SUBJECT}_archive
#mv anat/${SUBJECT}_struct_cropped.nii.gz ${SUBJECT}_archive

# cd dwi
#mv dwi/${SUBJECT}_dwi.bvals dwi/${SUBJECT}_dwi.bval
#mv dwi/${SUBJECT}_dwi.bvecs dwi/${SUBJECT}_dwi.bvec
# 	echo $f
#   mv $f sub-${f}
# done

# : <<COMMENTBLOCK
# Mango_bet structure for cleanup
# 9629:feckless:Data/BIP_ROTMAN> tree mango_bet
# mango_bet
# |-- MNI152_T1_2mm_brain.nii.gz
# |-- OC_mango_bet
# |   |-- sub-10306_B0.nii.gz
# |   |-- sub-10306_B0_brain_mask.nii.gz
# |   |-- sub-10306_B0_brain_mask_roi.nii.gz
# |   |-- sub-10306_struct_cropped.nii.gz
# |   |-- sub-10306_struct_mask.nii.gz
# |   |-- sub-10306_struct_mask_roi.nii.gz
#
# directory structure in main subject dir
# |-- anat
# |   |-- sub-10306_struct_cropped_defaced.log
# |   |-- sub-10306_struct_cropped_defaced.nii.gz
# |   `-- sub-10306_struct_mask.nii.gz
# |-- dwi
# |   |-- Bed
# |   |-- sub-10306_B0.nii.gz
# |   |-- sub-10306_B0_brain.nii.gz
# |   |-- sub-10306_B0_brain_mask.nii.gz
# |   |-- sub-10306_dwi.bval
# |   |-- sub-10306_dwi.bvec
# |   `-- sub-10306_dwi.nii.gz
#
# swap mask goals:
# mv anat/sub-10306_struct_mask.nii.gz sub-10306_archive/sub-10306_struct_mask_default.nii.gz
# mv mango_bet/sub-10306_struct_mask.nii.gz anat/
# mv dwi/sub-10306_B0_brain_mask.nii.gz sub-10306_archive/sub-10306_B0_brain_mask_default.nii.gz
# mv mango_bet/sub-10306_B0_brain_mask.nii.gz dwi/
# COMMENTBLOCK

#immv ${ANAT_MASK} ${ARCHIVE}/${SUBJECT}_struct_mask_default
#immv ${B0_MASK} ${ARCHIVE}/${SUBJECT}_B0_brain_mask_default
#MB=/Volumes/Main/Exps/Data/BIP_ROTMAN/mango_bet

#immv ${MB}/${SUBJECT}_struct_mask anat/
#immv ${MB}/${SUBJECT}_B0_brain_mask dwi/
#fslmaths anat/*defaced.nii.gz -mul anat/*mask.nii.gz ${ANAT_BRAIN}
#fslmaths dwi/*B0.nii.gz -mul dwi/*mask.nii.gz ${B0_BRAIN}

# if [[ ${VIEW} = "fsleyes" ]]; then
#   echo "eyes"
# elif [ ${VIEW} = "fslview" ] || [ ${VIEW} = "fslview_deprecated" ]; then
#   echo "old_viewer"
# fi

#${VIEW} ${B0} ${B0_MASK} -cm Red -a 25
#CheckBET_Anat


#ARCHIVE=/Volumes/Main/Exps/Data/BIP_ROTMAN/Archive_Rotman
# #ls ${ANAT_DERIV}
# #echo "${ARCHIVE}/${SUBJECT}/${SUBJECT}_T1w.anat/T1_fast_pve_0 ${CSF}"
# imcp ${ARCHIVE}/${SUBJECT}/${SUBJECT}_T1w.anat/T1_fast_pve_0 ${CSF}
# imcp ${ARCHIVE}/${SUBJECT}/${SUBJECT}_T1w.anat/T1_fast_pve_1 ${GM}
# imcp ${ARCHIVE}/${SUBJECT}/${SUBJECT}_T1w.anat/T1_fast_pve_2 ${WM}
# fslmaths ${CSF} ${CSF} -odt short
# fslmaths ${GM} ${GM} -odt short
# fslmaths ${WM} ${WM} -odt short
# fslmaths ${ANAT_MASK} ${ANAT_MASK} -odt short
# fslmaths ${ANAT_CROP} ${ANAT_CROP} -odt short
#ARCHIVE=/Volumes/Main/Exps/Data/BIP_ROTMAN/Archive_Rotman
#echo "archive: ${ARCHIVE}" #"/${SUBJECT}/${SUBJECT}_T1w_cropped"

# if [ ! -e ${ANAT_DERIV}/${SUBJECT}_T1w_cropped_deface_diff_mask.nii.gz ]; then
#  fslmaths ${ARCHIVE}/${SUBJECT}/${SUBJECT}_T1w_cropped -sub ${ANAT_DERIV}/${SUBJECT}_T1w_cropped_defaced.nii.gz  ${ANAT_DERIV}/${SUBJECT}_T1w_cropped_deface_diff.nii.gz -odt short
#
#  fslmaths ${ANAT_DERIV}/${SUBJECT}_T1w_cropped_deface_diff.nii.gz -bin ${ANAT_DERIV}/${SUBJECT}_T1w_cropped_deface_diff_mask.nii.gz -odt short
# fi
#imrm ${ANAT_DERIV}/${SUBJECT}_T1w_cropped_deface_diff.nii.gz

#imrm ${ANAT_DERIV}/${SUBJECT}*deface_diff.nii.gz

#mkdir ../sourcedata/${SUBJECT}
#mkdir ../derivatives/${SUBJECT}

# mv dwi.bval ${SUBJECT}_dwi.bval
# mv dwi.bvec ${SUBJECT}_dwi.bvec
# mv dwi.nii.gz ${SUBJECT}_dwi.nii.gz
# mv T1w.nii.gz ${SUBJECT}_T1w.nii.gz
# mkdir ../sourcedata/${SUBJECT}/anat
# mv ${SUBJECT}_T1w.nii.gz ../sourcedata/${SUBJECT}/anat
# mv ${SUBJECT}_T1w_defaced.nii.gz ${SUBJECT}_T1w.nii.gz
# mv *deface* ../sourcedata/${SUBJECT}/anat

mv dwi/${SUBJECT}_B0_brain_mask.nii.gz dwi/${SUBJECT}_B0_brain_mask_default.nii.gz
mv ../mango_bet/${SUBJECT}_B0_brain_mask_roi.nii.gz dwi/${SUBJECT}_B0_brain_mask.nii.gz
rm dwi/${SUBJECT}_B0_brain.nii.gz
fslmaths dwi/${SUBJECT}_B0 -mul dwi/${SUBJECT}_B0_brain_mask dwi/${SUBJECT}_B0_brain
}
Main
