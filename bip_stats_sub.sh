##!/bin/bash
#.bip_profile takes an argument that sets the project specific profile.
# This way we can source the project specific profile with a variable name.

source ${PROJECT_PROFILE}
#==============================================================================

: <<COMMENTBLOCK

###############################################################################
AUTHOR:         Dianne Patterson University of Arizona
DATE CREATED:   Today is 2/26/12. It is now 5:20 PM
----------------------------  DEPENDENCIES  -----------------------------------
The script is dependent on 'profiles' found in /usr/local/tools/REF/PROFILES.
(e.g., bip_img_profile.sh, bip_subject_profile.sh and project specific profiles). The
profiles define variable names and call bip_functions.sh. All project specific
profiles source the bip_subject_profile.sh which sources bip_img_profile.sh.

Must be run from the subject's main directory.
The results of running bip.sh must exist.
It uses bip_functions.sh: MkSublistArray and FindMax and calls getstats_dti.sh.
===============================================================================
----------------------------  PURPOSE  ----------------------------------------
Generate native space scalar volume, DTI values and Center of Gravity data
for access: for both final endpoints and final tracts.
Generated standard space images for parcellations and tracts.

See bip_stats.sh which calls this repeatedly for all the tracts.
===============================================================================
----------------------------  INPUT  ------------------------------------------
A directory containing the results of running bip.
===============================================================================
----------------------------  OUTPUT  -----------------------------------------
-Data in Access/access_endpoint_bip.txt. This contains volume, cog and dti values
for the endpoints.
-Data in Access/access_tract_bip.txt. This contains volume and dti values
for the tract extracted from the highest numbered Iteration directory (B if
there are an equal number of Iterations for A and B)
===============================================================================
----------------------------  CALLS   -----------------------------------------
From other scripts B02MNI, FindMax, MkSublistArray, MkDirIfNot, MyLog and
MNI2B0Mask from function.sh; get_stats_dti.sh, bip_ireg.sh
###############################################################################

COMMENTBLOCK

# paths
source bip_subject_profile.sh

###############################################################################
############################  DEFINE FUNCTIONS  ##################################
: <<COMMENTBLOCK

Function:   CleanAll
Purpose:    Remove files created by bip_stats_sub
Input:      Directory
Output:
Calls:      None

COMMENTBLOCK

CleanAll ()
{
if [ $side = "ih" ]; then
  thistract=${tract}
else
  thistract=${tract}_${side}
  echo "thistract is $thistract"
fi

rm ${IP}/fdt* ${IP}/stats* ${IP}/${thistract}*
rm ${BipS}/${thistract}*
rm ${STATS}/*bip*

}

#==============================================================================
: <<COMMENTBLOCK

Function:   CleanTracts
Purpose:    Remove tract files created by bip_stats_sub
Input:      Directory
Output:
Calls:      None

COMMENTBLOCK

CleanTracts ()
{
if [[ ${side} = "ih" ]]; then
  thistract=${tract}
else
  thistract=${tract}_${side}
  echo "thistract is ${thistract}"
fi

rm ${IP}/fdt* ${IP}/stats* ${IP}/${thistract}.nii.gz ${IP}/${thistract}_bin.nii.gz
rm ${BipS}/${thistract}*
rm ${STATS}/access_tract_bip.txt

}

#==============================================================================
: <<COMMENTBLOCK

Function:   DefineRegions
Purpose:    Get the sublist from lst_tract_seed_target_masks.txt.
            Define the parcellations
Input:      Tract name and side
Output:     Variable names
Calls:      MkSublistArray from bip_functions.sh

COMMENTBLOCK

DefineRegions ()
{
if [ $side = "ih" ]; then
    thistract=${tract}
  else
    thistract=${tract}_${side}
    echo "working on ${thistract}"
fi

MkSublistArray lst_tract_seed_target_masks.txt ${thistract}
roi1=${sublist_array[1]}
roi2=${sublist_array[2]}
roiA=`basename -s _diff_bin_csf.nii.gz ${roi1}`
roiB=`basename -s _diff_bin_csf.nii.gz ${roi2}`
IP=${DWI_DERIV}/${thistract}_bip
BipS=${DWI_DERIV}/Bip_stand

}

#==============================================================================

: <<COMMENTBLOCK

Function:   HelpMessage
Purpose:    Print relevant help message
Calls:      None

COMMENTBLOCK

HelpMessage ()
{
if [ $# -lt 2 ]; then
  echo "Usage: $0 <FunctionName> <tract> <side>"
  echo "Example: $0 Main arc r"
  echo "You need to enter the function to be run. e.g., Main, CleanAll"
  echo "side is l (left), r (right), or ih (interhemispheric)"
  exit 1
fi

echo "================================ "
}

#==============================================================================

: <<COMMENTBLOCK

Function:   LogEndpointStats
Purpose:    Put Native and standard space and standard space GM parcellation statistics,
			      including center of gravity and scalar dwi values into Access/access_endpoint_bip.txt
Input:      Parcellation A or B, bip dir must exist
Output:     Statistics in Access/access_endpoint_bip.txt
Calls:      FindMax from bip_functions.sh, get_stats_dwi.sh

COMMENTBLOCK

LogEndpointStats ()
{

AB=$1

# This generates the correct value of $i (the last iteration)
FindMax ${IP}/roi_${AB}_
# getstats_dwi creates stats.txt
echo "${IP}/roi_${AB}_${i}"
getstats_dti.sh ${IP}/roi_${AB}_${i}
mv stats.txt ${IP}/stats${AB}.txt
 # Get volume of the standard space mask in cubic mm
volmm_stand=`fslstats ${BipS}/${thistract}_${AB}_stand_bin -V | awk '{printf $2 "\n"}'`
# Get the volume of the standard space gm in cubic mm
volmm_stand_gm=`fslstats ${BipS}/${thistract}_${AB}_stand_gm_bin  -V | awk '{printf $2 "\n"}'`

# When I output the stats to a text file, I need to echo the correct roi
if [  ${AB} = "A" ]; then
    roi=${roiA}
  else
    roi=${roiB}
fi

# Get center of gravity for parcellation for native space, standard space, and standard space gm
cog=`fslstats ${IP}/roi_${AB}_${i} -C`
cog_stand=`fslstats ${BipS}/${thistract}_${AB}_stand_bin -C`
cog_stand_gm=`fslstats ${BipS}/${thistract}_${AB}_stand_gm_bin -C`

if [ ! -e ${STATS}/access_endpoint_bip.txt ]; then
  touch ${STATS}/access_endpoint_bip.txt
  echo Subj tract R_L roi AB volmm_stand volmm_stand_gm volmm_native MD FA RD PD MO x_vox_native \
  y_vox_native z_vox_native x_vox_stand y_vox_stand z_vox_stand x_vox_gm_stand y_vox_gm_stand z_vox_gm_stand \
  > ${STATS}/access_endpoint_bip.txt
fi

echo ${SUBJECT} ${tract} ${side} ${roi} ${AB} ${volmm_stand} ${volmm_stand_gm} `cat ${IP}/stats${AB}.txt` \
${cog} ${cog_stand} ${cog_stand_gm} >> ${STATS}/access_endpoint_bip.txt
}

#==============================================================================

: <<COMMENTBLOCK

Function:   LogEndpointStats2
Purpose:    Create Native space GM parcellation statistics,
            including center of gravity and scalar dwi values into Access/access_endpoint_bip2.txt
Input:      Parcellation A or B, bip dir must exist.  LogEndpointStats must have been run
Output:     Statistics in Access/access_endpoint_bip2.txt
Calls:      None

COMMENTBLOCK

LogEndpointStats2 ()
{
AB=$1


echo "${IP}/${thistract}_${AB}_nat_gm_bin"
# get the volume of the gm native space parcellation
volmm_nat_gm=`fslstats ${IP}/${thistract}_${AB}_nat_gm_bin -V | awk '{printf $2 "\n"}'`

# When I output the stats to a text file, I need to echo the correct roi
if [  ${AB} = "A" ]; then
    roi=${roiA}
  else
    roi=${roiB}
fi

# Get center of gravity for parcellation for native space, standard space, and standard space gm
cog_gm=`fslstats ${IP}/${thistract}_${AB}_nat_gm_bin -C`

if [ ! -e ${STATS}/access_endpoint_bip2.txt ]; then
  touch ${STATS}/access_endpoint_bip2.txt
  echo Subj tract R_L roi AB volmm_native_gm x_vox_nat_gm y_vox_nat_gm z_vox_nat_gm \
  > ${STATS}/access_endpoint_bip2.txt
fi

echo ${SUBJECT} ${tract} ${side} ${roi} ${AB} ${volmm_nat_gm} \
${cog_gm} >> ${STATS}/access_endpoint_bip2.txt
}

#==============================================================================

: <<COMMENTBLOCK

Function:   LogEndpointStats3
Purpose:    Create Native space GM parcellation statistics,
            including center of gravity and scalar dwi values into Access/access_endpoint_bip2.txt
Input:      Parcellation A or B, bip dir must exist.  LogEndpointStats must have been run
Output:     Statistics in Access/access_endpoint_bip2.txt
Calls:      None

COMMENTBLOCK

LogEndpointStats3 ()
{
AB=$1

echo "${IP}/${thistract}_${AB}_nat_gm_bin"

# When I output the stats to a text file, I need to echo the correct roi
if [  ${AB} = "A" ]; then
    roi=${roiA}
  else
    roi=${roiB}
fi

volmm_nat=`fslstats ${IP}/${thistract}_${AB}_bin -V | awk '{printf $2 "\n"}'`
volmm_nat_gm=`fslstats ${IP}/${thistract}_${AB}_nat_gm_bin -V | awk '{printf $2 "\n"}'`

if [ ! -e ${STATS}/access_endpoint_bip3.txt ]; then
  touch ${STATS}/access_endpoint_bip3.txt
  echo Subj tract R_L roi AB volmm_nat volmm_native_gm  > ${STATS}/access_endpoint_bip3.txt
fi

echo ${SUBJECT} ${tract} ${side} ${roi} ${AB} ${volmm_nat} ${volmm_nat_gm} >> ${STATS}/access_endpoint_bip3.txt
}
#==============================================================================
: <<COMMENTBLOCK

Function:   LogTractStats
Purpose:    Put Native space tract statistics, into Access/access_tract_bip.txt
Input:      Must have run MkTracts
Output:     Statistics in Access/access_tract_bip.txt: native and standard volumes.
            DWI measures on native space stuff.
Calls:      None

COMMENTBLOCK

LogTractStats ()
{
getstats_dti.sh ${IP}/${thistract}
mv stats.txt ${IP}
# Get volume in standard space in cubic mm
volmm_tract_stand=`fslstats ${BipS}/${thistract}_stand_bin -V | awk '{printf $2 "\n"}'`
volmm_tract_wm_stand=`fslstats ${BipS}/${thistract}_stand_wm_bin -V | awk '{printf $2 "\n"}'`
if [ ! -e ${STATS}/access_tract_bip.txt ]; then
  touch ${STATS}/access_tract_bip.txt
  echo Subj tract R_L volmm_tract_stand volmm_tract_wm_stand volmm_tract_native  MD FA RD PD MO > ${STATS}/access_tract_bip.txt
fi
echo ${SUBJECT} ${tract} ${side} ${volmm_tract_stand} ${volmm_tract_wm_stand} `cat ${IP}/stats.txt` >> ${STATS}/access_tract_bip.txt
}

#==============================================================================
: <<COMMENTBLOCK

Function:   LogTractStats2
Purpose:    Put Native space tract statistics, into Access/access_tract_bip2.txt
Input:      Must have run MkTracts2
Output:     Statistics in Access/access_tract_bip2.txt: native and standard volumes.
            DWI measures on native space stuff.  This is a better measure of tract statistics
            than LogTractStats because we get all the white matter rather than truncating it at the parcellations.
Calls:      getstats_dwi.sh

COMMENTBLOCK

LogTractStats2 ()
{
getstats_dti.sh ${IP}/${thistract}_all_wm
mv stats.txt ${IP}
# Get volume in native space in cubic mm
volmm_tract=`fslstats ${IP}/${thistract}_all_wm -V | awk '{printf $2 "\n"}'`
volmm_tract_stand_all_wm_bin=`fslstats ${BipS}/${thistract}_stand_all_wm_bin -V | awk '{printf $2 "\n"}'`
if [ ! -e ${STATS}/access_tract_bip2.txt ]; then
  touch ${STATS}/access_tract_bip2.txt
  echo Subj tract R_L volmm_tract_stand_all_wm volmm_tract_wm MD FA RD PD MO > ${STATS}/access_tract_bip2.txt
fi
echo ${SUBJECT} ${tract} ${side} ${volmm_tract_stand_all_wm_bin} `cat ${IP}/stats.txt` >> ${STATS}/access_tract_bip2.txt
}

#==============================================================================
: <<COMMENTBLOCK

Function:   MkEndpoints
Purpose:    Make standard space binary endpoints
Input:      The tract bip dir must exist and contain the rois
Output:     Standard parcellation (endpoint) masks in ${BipS}
Calls:      getstats_dti.sh; bip_ireg.sh; FindMax in bip_functions.sh,

COMMENTBLOCK

MkEndpoints ()
{
for AB in A B; do
  if [ ! -e ${BipS}/${thistract}_${AB}_stand_bin.nii.gz ]; then
    # Identify the max Iteration (i may be different for
    # Iteration dir than for roi)
    FindMax ${IP}/roi_${AB}_
    echo "Iteration ${AB} is $i"
    # Copy each max roi to tract name_A or _B
    imcp ${IP}/roi_${AB}_${i} ${IP}/${thistract}_${AB}
    # Create binary versions of the parcellations
    fslmaths ${IP}/${thistract}_${AB} -bin ${IP}/${thistract}_${AB}_bin
    # Register parcellation masks into standard space
    bip_ireg.sh -m ${IP}/roi_${AB}_${i} ${stand_brain} ${REG}/diff2standard.mat ${BipS}/${thistract}_${AB}_stand
  else
     echo "Parcellation image files already created.  Moving on."
  fi
done
}

#==============================================================================
: <<COMMENTBLOCK

Function:   MkEndpoints2
Purpose:    Make standard space binary endpoints
Input:      The tract bip dir must exist and contain the rois
Output:     Standard parcellation (endpoint) masks in ${BipS} for the new BBR registered images
Calls:      FindMax and B02MNI from bip_functions.sh;

COMMENTBLOCK

MkEndpoints2 ()
{
for AB in A B; do
  if [ ! -e ${BipS}/${thistract}_${AB}_stand_bin.nii.gz ]; then
    # Identify the max Iteration (i may be different for
    # Iteration dir than for roi)
    FindMax ${IP}/roi_${AB}_
    echo "Iteration ${AB} is $i"
    # Copy each max roi to tract name_A or _B
    imcp ${IP}/roi_${AB}_${i} ${IP}/${thistract}_${AB}
    # Create binary versions of the parcellations
    fslmaths ${IP}/${thistract}_${AB} -bin ${IP}/${thistract}_${AB}_bin
    # Register parcellation masks into standard space
    B02MNI ${IP}/roi_${AB}_${i} ${BipS}/${thistract}_${AB}_stand
    fslmaths ${BipS}/${thistract}_${AB}_stand -bin ${BipS}/${thistract}_${AB}_stand_bin
  else
    echo "Parcellation image files already created.  Moving on."
  fi
done
}

#==============================================================================
: <<COMMENTBLOCK

Function:   MkGMEndpoints
Purpose:    Make standard space binary endpoints (cortical ribbon only)
Input:      The  Standard parcellation (endpoint) masks in ${BipS}
Output:     GM masked standard parcellation (endpoint) masks in ${BipS}
Calls:      None

COMMENTBLOCK

MkGMEndpoints ()
{
for AB in A B; do
  if [ ! -e ${BipS}/${thistract}_${AB}_stand_gm_bin.nii.gz ]; then
    #create the volume of just the gm
    echo ${mask_stand_gm}
 fslmaths ${BipS}/${thistract}_${AB}_stand_bin -mas ${mask_stand_gm} -bin ${BipS}/${thistract}_${AB}_stand_gm_bin
  else
    echo "GM Parcellation image files already created.  Moving on."
  fi
done
}

#==============================================================================
: <<COMMENTBLOCK

Function:   MkNativeGMEndpoints
Purpose:    Make native space binary endpoints (cortical ribbon only)
Input:      The  dti parcellation (endpoint) masks in each tract dir
Output:     GM masked native parcellation (endpoint) masks in ${IP}
Calls:      None

COMMENTBLOCK

MkNativeGMEndpoints ()
{
for AB in A B; do
    if [ ! -e  ${IP}/${thistract}_${AB}_nat_gm_bin.nii.gz ]; then
    	#create the volume of just the gm
	 fslmaths ${IP}/${thistract}_${AB}_bin -sub Morph/${SUBJECT}_dti_wm_bin -bin ${IP}/${thistract}_${AB}_nat_gm_bin
    else
       echo "Native GM Parcellation image files already created.  Moving on."
    fi
done
}

#==============================================================================
: <<COMMENTBLOCK

Function:   MkNativeGMEndpoints2
Purpose:    Make native space binary endpoints (cortical ribbon only)
Input:      The  dti parcellation (endpoint) masks in each tract dir
Output:     GM masked native parcellation (endpoint) masks in ${IP}
Calls:      None

COMMENTBLOCK

MkNativeGMEndpoints2 ()
{
for AB in A B; do
  if [ ! -e  ${IP}/${thistract}_${AB}_nat_gm_bin.nii.gz ]; then
    #create the volume of just the gm
    fslmaths ${IP}/${thistract}_${AB}_bin -sub ${SUBROIS}/wm_diff_bin -bin ${IP}/${thistract}_${AB}_nat_gm_bin
  else
    echo "Native GM Parcellation image files already created.  Moving on."
  fi
done
}
#==============================================================================
: <<COMMENTBLOCK

Function:   MkSeedEnds
Purpose:    Make standard space binary seed endpoints for each subject for brocas and wernickes
            This is so we can make a figure showing the initial seed regions bip starts with
Input:      The tract bip dir must exist and contain the rois
Output:     Standard space seed parcellation (endpoint) masks in Bip_hotspot/Seeds
Calls:      None

COMMENTBLOCK

MkSeedEnds ()
{
cp ${SUBROIS}/wernickes5_l_diff_bin_csf ${D}/Bip_hotspot/Seeds/seed_${SUBJECT}_spt_l
cp ${SUBROIS}/wernickes5_r_diff_bin_csf ${D}/Bip_hotspot/Seeds/seed_${SUBJECT}_spt_r
cp ${SUBROIS}/brocas3_l_diff_bin_csf ${D}/Bip_hotspot/Seeds/seed_${SUBJECT}_ifg_l
cp ${SUBROIS}/brocas3_r_diff_bin_csf ${D}/Bip_hotspot/Seeds/seed_${SUBJECT}_ifg_r

}

#==============================================================================

: <<COMMENTBLOCK

Function:   MkTracts
Purpose:    Create thresholded and binary versions of tracts.
            Threshold the average of the 2 tracts at 5p.
            Subtract endpoints from tract.
            Create both native and standard space output images
            Also create an all and only wm standard space version of the tract
Input:      Parcellation A or B, bip dir must exist
Output:     fdt* images,
            tract image (thresholded at 5p),
            tract image (binary)
            tract image (standard space binary)
            tract image (standard space, wm)
Calls:      FindMax from bip_functions.sh; bip_ireg.sh

COMMENTBLOCK

MkTracts ()
{
# If the tract image does NOT exist in the tract dir. Create it.
for AB in A B; do
  if [ ! -e ${IP}/fdt_${AB} ]; then
    # Identify the max Iteration (i may be different for
    # Iteration dir than for roi)
    FindMax ${IP}/Iteration_${AB}_
    echo "Iteration ${AB} is $i"
    # Create a copy of the tract for the max iteration
    # in each direction (A or B) in the tract dir.
    # Call them fdt_A and fdt_B
    imcp ${IP}/Iteration_${AB}_${i}/fdt_paths ${IP}/fdt_${AB}
  else
    echo "${IP}/fdt_${AB}  already exists.  Moving on."
  fi
done

# original parcellation masks defined in masks.txt
# maskA=`head -n 1 ${IP}/masks.txt`
# maskB=`tail -n 1 ${IP}/masks.txt`
if [ ! -e ${IP}/${thistract} ]; then
  # Average the 2 tracts together and create fdt.nii.gz
  fslmaths ${IP}/fdt_A -add ${IP}/fdt_B -div 2 ${IP}/fdt
  # Now threshold the average image fdt at 5p
  fslmaths ${IP}/fdt -thrp 5 ${IP}/fdt_5p
  # Create a binary version
  fslmaths ${IP}/fdt_5p -bin ${IP}/fdt_bin
  # Subtract best parcellation masks from tract mask to create new tract mask
  fslmaths ${IP}/fdt_bin -sub ${IP}/${thistract}_A_bin \
  -sub ${IP}/${thistract}_B_bin -bin ${IP}/${thistract}_bin
  # Create native space tract that has no contribution from the parcellations (truncated tract)
  fslmaths ${IP}/fdt_5p -mul ${IP}/${thistract}_bin ${IP}/${thistract}
fi
# Create the standard space version of the truncated tract, if it does not exist
if [ ! -e ${BipS}/${thistract}_stand_bin.nii.gz ]; then
  bip_ireg.sh -m ${IP}/${thistract}_bin ${stand_brain} \
  ${REG}/diff2standard.mat ${BipS}/${thistract}_stand
else
  echo "Standard truncated tract image files already exist.  Moving on."
fi

# Create the standard space WM version of the truncated tract, if it does not exist
if [ ! -e ${BipS}/${thistract}_stand_wm_bin.nii.gz ]; then
  fslmaths ${BipS}/${thistract}_stand_bin -add ${BipS}/${thistract}_A_stand_bin \
  -add ${BipS}/${thistract}_B_stand_bin -mul ${mask_stand_wm} \
  -bin ${BipS}/${thistract}_stand_wm_bin
else
  echo "Standard truncated wm tract image files already exist.  Moving on."
fi

}

#==============================================================================
: <<COMMENTBLOCK

Function:   MkTracts2
Purpose:    Create thresholded and binary versions of tracts.
            Threshold the average of the 2 tracts at 5p.
            Subtract endpoints from tract.
            Create both native and standard space output images
            Also create an all and only wm standard space version of the tract
Input:      Parcellation A or B, bip dir must exist, Mktracts must have been run first
Output:     Tract image (binary trimmed to native wm mask)
            Tract image (standard space binary, wm_all)
Calls:      bip_ireg.sh


COMMENTBLOCK

MkTracts2 ()
{
if [ ! -e ${IP}/${thistract}_all_wm.nii.gz ]; then
  # Create native space wm only tract mask (parcellation contributions allowed if they are wm)
  fslmaths ${IP}/fdt_bin -mul ${SUBROIS}/wm_diff_bin ${IP}/${thistract}_all_wm
fi

# Create the standard space version of the all_wm tract, if it does not exist
# Because we do not trust the segmentation in native space, we are going to
# register fdt_bin to standard space, and then trim it to include only white matter
# in the standard space white matter segmentation.

if [ ! -e ${BipS}/${thistract}_stand_all_wm_bin.nii.gz ]; then
  bip_ireg.sh -m ${IP}/fdt_bin ${stand_brain} \
  ${REG}/diff2standard.mat ${BipS}/${thistract}_stand_all_wm
  fslmaths ${BipS}/${thistract}_stand_all_wm_bin -mul ${mask_stand_wm} -bin ${BipS}/${thistract}_stand_all_wm_bin
else
  echo "Standard wm_all tract image files already exist.  Moving on."
fi
}

#==============================================================================

: <<COMMENTBLOCK

Function:   MkTractsBBR
Purpose:    Create thresholded and binary versions of tracts.
            Threshold the average of the 2 tracts at 5p.
            Subtract endpoints from tract.
            Create both native and standard space output images
            Also create an all and only wm standard space version of the tract
Input:      Parcellation A or B, bip dir must exist
            Registrations created using BBR
Output:     fdt* images,
            tract image (thresholded at 5p),
            tract image (binary)
            tract image (standard space binary)
            tract image (standard space, wm)
Calls:      FindMax and B02MNI from bip_functions.sh

COMMENTBLOCK

MkTractsBBR ()
{
# If the tract image does NOT exist in the tract dir. Create it.
for AB in A B; do
  if [ ! -e ${IP}/fdt_${AB} ]; then
    # Identify the max Iteration (i may be different for
    # Iteration dir than for roi)
    FindMax ${IP}/Iteration_${AB}_
    echo "Iteration ${AB} is $i"
    # Create a copy of the tract for the max iteration
    # in each direction (A or B) in the tract dir.
    # Call them fdt_A and fdt_B
    imcp ${IP}/Iteration_${AB}_${i}/fdt_paths ${IP}/fdt_${AB}
  else
    echo "${IP}/fdt_${AB} already exists. Moving on."
  fi
done

# original parcellation masks defined in masks.txt
# maskA=`head -n 1 ${IP}/masks.txt`
# maskB=`tail -n 1 ${IP}/masks.txt`
if [ ! -e ${IP}/${thistract} ]; then
  # Average the 2 tracts together and create fdt.nii.gz
  fslmaths ${IP}/fdt_A -add ${IP}/fdt_B -div 2 ${IP}/fdt
  # Now threshold the average image fdt at 5p
  fslmaths ${IP}/fdt -thrp 5 ${IP}/fdt_5p
  # Create a binary version
  fslmaths ${IP}/fdt_5p -bin ${IP}/fdt_bin
  # Subtract best parcellation masks from tract mask to create new tract mask
  fslmaths ${IP}/fdt_bin -sub ${IP}/${thistract}_A_bin \
  -sub ${IP}/${thistract}_B_bin -bin ${IP}/${thistract}_bin
  # Create native space tract that has no contribution from the parcellations (truncated tract)
  fslmaths ${IP}/fdt_5p -mul ${IP}/${thistract}_bin ${IP}/${thistract}
fi
# Create the standard space version of the truncated tract, if it does not exist
if [ ! -e ${BipS}/${thistract}_stand_bin.nii.gz ]; then
  #bip_ireg.sh -m ${IP}/${thistract}_bin ${stand_brain} \
  #${REG}/diff2standard.mat ${BipS}/${thistract}_stand
  B02MNI ${IP}/${thistract}_bin ${BipS}/${thistract}_stand
  fslmaths ${BipS}/${thistract}_stand -bin ${BipS}/${thistract}_stand_bin
else
  echo "Standard truncated tract image files already exist.  Moving on."
fi

# Create the standard space WM version of the truncated tract, if it does not exist
if [ ! -e ${BipS}/${thistract}_stand_wm_bin.nii.gz ]; then
  fslmaths ${BipS}/${thistract}_stand_bin -add ${BipS}/${thistract}_A_stand_bin \
  -add ${BipS}/${thistract}_B_stand_bin -mul ${mask_stand_wm} \
  -bin ${BipS}/${thistract}_stand_wm_bin
else
  echo "Standard truncated wm tract image files already exist.  Moving on."
fi

}

#==============================================================================
: <<COMMENTBLOCK

Function:   MkTractsBBR2
Purpose:    Create thresholded and binary versions of tracts.
            Threshold the average of the 2 tracts at 5p.
            Subtract endpoints from tract.
            Create both native and standard space output images
            Also create an all and only wm standard space version of the tract
Input:      Parcellation A or B, bip dir must exist, Mktracts must have been run
            first.
			      Registrations created using BBR
Output:     tract image (binary trimmed to native wm mask)
            tract image (standard space binary, wm_all)
Calls:      B02MNI from bip_functions.sh

COMMENTBLOCK

MkTractsBBR2 ()
{
if [ ! -e ${IP}/${thistract}_all_wm.nii.gz ]; then
  # Create native space wm only tract mask (parcellation contributions allowed if they are wm)
  fslmaths ${IP}/fdt_bin -mul ${SUBROIS}/wm_diff_bin ${IP}/${thistract}_all_wm
fi

# Create the standard space version of the all_wm tract, if it does not exist
# Because we do not trust the segmentation in native space, we are going to
# register fdt_bin to standard space, and then trim it to include only white matter
# in the standard space white matter segmentation.

if [ ! -e ${BipS}/${thistract}_stand_all_wm_bin.nii.gz ]; then
  #bip_ireg.sh -m ${IP}/fdt_bin ${stand_brain} \
  #${REG}/diff2standard.mat ${BipS}/${thistract}_stand_all_wm
  B02MNI ${IP}/fdt_bin ${BipS}/${thistract}_stand_all_wm
  fslmaths ${BipS}/${thistract}_stand_all_wm -bin ${BipS}/${thistract}_stand_all_wm_bin
  fslmaths ${BipS}/${thistract}_stand_all_wm_bin -mul ${mask_stand_wm} -bin ${BipS}/${thistract}_stand_all_wm_bin
else
  echo "Standard wm_all tract image files already exist.  Moving on."
fi
}

#==============================================================================

: <<COMMENTBLOCK

Function:   Main
Purpose:    Run native and standard space statistics
Input:      tract_bip directory where bip.sh was run
Output:     fdt* images,
            tract image (thresholded at 5p),
            tract and endpoint stats: stats.txt, statsA.txt and statsB.txt
            endpoint images in standard space (in DTI/Bip_stand)
            tract images in standard space
Calls:      MkDirIfNot from bip_functions.sh; 9 functions from this script
COMMENTBLOCK

Main ()
{
MkDirIfNot ${DWI_DERIV}/Bip_stand

# Call Functions
MkEndpoints                                  # Make standard space binary versions of each parcellation in Bip_stand
MkGMEndpoints                                # Make GMmasked version of Endpoints for each parcellation in Bip_stand
MkNativeGMEndpoints                          # Make native space GM masked version of parcellations
LogEndpointStats A                           # Get stats for parcellation A
LogEndpointStats B                           # Get stats for parcellation B
LogEndpointStats2 A                          # Get stats for parcellation A
LogEndpointStats2 B                          # Get stats for parcellation B
MkTracts                                     # Make thresholded, binary and
                                             # standard space versions of tract
MkTracts2								                     # tract values for non-truncated tracts.
LogTractStats                                # Get stats for truncated tracts
LogTractStats2                               # Get stats for better tracts
}

#==============================================================================
: <<COMMENTBLOCK

Function:   Main2
Purpose:    Run Native and Standard space statistics for BBR registered images (Siemens vs GE)
Input:
Output:
Calls:      MkDirIfNot from bip_functions.sh; 9 functions from this script

COMMENTBLOCK

Main2 ()
{

MkDirIfNot ${DWI_DERIV}/Bip_stand
if [ -e ${IP}/biplog_${thistract}.txt ]; then
# Call Functions
MkEndpoints2                                 # Make standard space binary versions of each parcellation in Bip_stand
MkGMEndpoints                               # Make GMmasked version of Endpoints for each parcellation in Bip_stand
MkNativeGMEndpoints2                        # Make native space GM masked version of parcellations
LogEndpointStats A                          # Get stats for parcellation A
LogEndpointStats B                          # Get stats for parcellation B
LogEndpointStats2 A                         # Get stats for parcellation A
LogEndpointStats2 B                         # Get stats for parcellation B
MkTractsBBR                                 # Make thresholded, binary and
#                                           # standard space versions of tract
MkTractsBBR2                                # tract values for non-truncated tracts.
LogTractStats                               # Get stats for truncated tracts
LogTractStats2                              # Get stats for better tracts
fi

}
#########################  END FUNCTION DEFINITIONS  ##########################
###############################################################################

while getopts h options 2> /dev/null; do        		# Setup -h flag. redirect errors
  case $options in                                			# In case someone invokes -h
  h) HelpMessage;;                                		# Run the help message function
  \?) echo "only h is a valid flag" 1>&2          		# If bad options are passed, print message
  esac
done

shift $(( $OPTIND -1))                          			# Index option count, so h isn't treated as arg
optnum=$(( $OPTIND -1))                         		# optnums hold # of options passed in

if [ ${optnum} -lt 1 -a $# -eq 3 ]; then        		# If no options
dothis=$1                                       			# then first arg is function
tract=$2
side=$3
else
 HelpMessage                                         			# 2nd arg is tract
fi
DefineRegions ${tract} ${side}           			# Define variable names

$dothis ${tract} ${side}                        			# Execute requested function

MyLog                                           			# Log the activity
