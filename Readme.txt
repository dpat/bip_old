Dianne Patterson, University of Arizona, SLHS Dept
Created Feb 23, 2017
Updated Nov 14, 2017

Special thanks to Patricia Klobusiakova without whose eagle eye and
sharp mind, there would be many more errors in this set of scripts.
=====================================================
DIRECTORY DESCRIPTION
This directory contains bash scripts, profiles, lists,
and standard space image masks used to run bip and associated stats.

The scripts primarily run FSL 5.0.10 commands (these should be backward compatible to ~5.0.7).
Everything was written to run under OSX Sierra, and tested under linux and Mac OSX.
----------------------------------------------------
LIBRARY: contains scripts containing shared functions.
REF contains useful binary masks derived from standard atlases.

PROFILES: Contains "profiles" that define global variables.
bip_profile should be sourced and provided with one argument: one of the project-specific profiles in this directory.  Project-specific profiles set FILE DESCRIPTOR SETTINGS and PARAMETER VALUES. By default the naming structure emulates the BIDS standard, as defined in the bids_profile.sh

REF: Contains non-scripts:
  DWI contains parameter files (for eddy and topup),
  LISTS contains lists of subjects and rois to facilitate iteration
  IMAGES contains binary image masks for particular tracts.

=====================================================
REVISION CONTROL
The directory structure is under version control using git.

git clone https://dpat@bitbucket.org/dpat/bip.git
to create a copy of the tools. After this run
>git init .
-Subsequently, git push and pull should work.
=====================================================
OVERVIEW OF STEPS
=====================================================
GETTING READY
Begin with a 4D dti image and the accompanying bvals and bvecs in FSL format,
a structural image (e.g., an SPGR or MPRAGE).

Optionally, if you have the fieldmaps (magnitude and phase images),
you can use these to improve undistortion and registration.

If you have reverse phase encoded images of the DWI data
(even reverse-phase encoded B0s) you can use these additional files with topup,
prior to running eddy, to further improve your distortion.

Also include a lesion mask if you have one (1's in the lesion, 0's elsewhere)

A) Define Project-specific profiles for FILE DESCRIPTOR SETTINGS and PARAMETER VALUES
All profiles are contained in ${BIP_HOME}/PROFILES.  Examples of project specific profiles
for the sample directories include bip_profile_fm.sh, bip_profile_fmTop.sh and bip_profile_min.sh.  Others could be created based on these examples. The project specific profile sources the bids_profile.

B) Source your project-specific profile (it must be in your path)
Example bash alias from the .bashrc (assumes ${BIP_HOME} is defined).  Add this to your .bashrc:
export BIP_HOME="/vol_c/bip"
# Example alias for Cyverse machine:
alias sbip_min='source ${BIP_HOME}/PROFILES/bip_cy_profile ${BIP_HOME}/PROFILES/bip_profile_min.sh'

Now, if you type the following in the Cyverse shell, your environment will be set up in that shell:
>sbip_min

N.B. If you want to process data on a non-hipaa compliant compute resource, then
you should run deface.sh on any structural files....and check that the resulting files, e.g., :
>deface.sh anat/sub-001_mprage.

Check that the processing has retained all brain voxels:
>check.sh Deface
------------------------------------------------------
RUNNING THE SCRIPTS
1) Run precheck.sh from inside a subject directory.
This will insure the correct files exist (and have the expected names as specified in your profile).
It will report your project profile name and the viewer you have chosen.

2) Run setup.sh from inside a subject directory.
setup.sh will set up the directory structure and do skull stripping.
After this step you can manually check masks, alter them, and regenerate extracted brains and or segmentations. I particularly recommend that you check the masks for the B0 image.

The function PrepMPRAGE (see bip/LIBRARY/bip_functions.sh) runs fsl_anat and some registrations. It takes ~15 minutes to run on a 2013 MacPro.

If you have reverse polarity images, then setup.sh will also run the function PrepDWI (see bip/LIBRARY/bip_functions.sh) which does topup and applytopup. This takes ~25 minutes to run on a 2013 mac pro. topup calculates the distortion and movement in the DWI images.

applytopup applies the distortion and movement corrections to the B0 images.
applytopup works like this: we have 2 images:
blip_up consists of 2 B0 blip up volumes.  Call these volumes 1 and 2.
blip_down consists of 2 B0 blip down volumes.  Call these volumes 3 and 4.
applytopup combines the first blip_up volume (1) with the first blip_down volume (3) to create a volume (1+3). Then the 2nd blip_up image (2) is combined with the second blip_down image (4) to create a volume (2+4). These two new volumes are stored in B0_HIFI.
==================================
#### Work needed here!! We need a resegmentation routine??####
Run fslmaths input -mul newmask output.
e.g. fslmaths my_mprage -mul mprage_mask my_mprage
(this avoids changing the names of the files)
==================================
3) Run prep.sh from inside a subject directory.
prep.sh will correct the dwi image (with eddy_correct or eddy depending on your optional files).

prep.sh will then run epi_reg to register your DWI B0 image into structural space (~ 5 minutes).
epi_reg requires a phase encode direction (--pedir).  See function BBRDWI in bip_functions.
We default to -y. This seems optimal for A-P DWI images acquired in a Siemens skyra.

Finally, prep.sh will run dtifit and BedpostX (BedpostX can take days to run. It benefits from a good graphics card and more CPUs.  More directions take longer to process). The shortest time I've seen is about 5 hours.

4) Run
>check.sh Reg
from inside a subject directory, This will use the registration files to create test images, which can then be reviewed.

5) Run bip_prep.sh -h to see the help.
This will tell you all possible tracts with rois and masks set up for bip.
If you run bip_prep.sh with no arguments, it generates the endpoint rois and inverse masks in subjectrois and also generates directories under dwi for every possible tract you may wish to run with bip.
Alternatively, you can supply an argument to set up particular tracts, e.g.,
>bip_prep.sh arc_l
The choice of tracts is listed in the help. You can run bip_prep.sh repeatedly to set up the tracts of interest.

6) Run bip.sh on the tract of interest. This uses probtrackx2 (which is faster than probtrackx, and has more options for generating network matrices). Depending on the tract (especially how big the endpoints are), this takes from several hours to several days.
>bip.sh arc_l

6.5) bip_old.sh runs the original probtrackx code.  Note the results of bip.sh and bip_old.sh are different!!

#####################################################
FIELDMAP AND TOPUP PARAMETER FILES:
#####################################################
If using field maps or reverse-phase encode image, certain parameters need to be set:

####### Field maps ################
Field maps (about 1 minute) require the Difference in Echo times, which is usually 2.46 by default on the Siemens.

https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FUGUE/Guide#SIEMENS_data

Acquiring and Using Fieldmaps: https://lcni.uoregon.edu/kb-articles/kb-0003

#### DIFFERENCE IN ECHO TIMES ####
Needed for fsl_prepare_fieldmap, and listed as ECHO_DIFF in bip_subject_profile.
By default this is 2.46 for Siemens, and refers to characteristics of the fieldmaps.

To calculate the difference in echo times, you need the TE for the first and 2nd echo:
TE=4.92 (first mag); TE=7.38 (phase and 2nd mag); 1 echo (First mag map); 2 echoes (phase map and 2nd mag); 7.38-4.92=2.46
-------
If you use dcm2niix with the -b option (for BIDS), then a *.json file is created for each image. Although difference in echo times is relatively easy to calculate, it could also be calculated automatically from the json files:

# The first image in the directory should be a magnitude image
mag_vol1=`ls -1 fmap | head -n 1`
# The last image in the directory should be the phase image
phase=`ls -1 fmap | tail -n 1`
# Get the basenames of the files in order to construct the names of the json files
mag1_stem=`basename -s .nii.gz ${mag_vol1}`
phase_stem=`basename -s .nii.gz ${phase}`

# Find the echotime values in the json files
ET1=`cat json/${mag1_stem}.json | grep -m 1 EchoTime | sed 's/,//g' | awk '{print $2}'`
ET2=`cat json/${phase_stem}.json | grep -m 1 EchoTime | sed 's/,//g' | awk '{print $2}'`

# Calculate the value to use in the siemens fieldmapping
echodiff=`echo "scale=2; (${ET2} - ${ET1}) * 1000" | bc`
echo "echodiff is ${echodiff}"
-------
(For other scanners, consult the FSL page above.)

#### EFFECTIVE ECHO SPACING ####
This is ECHO_SPACE in bip_subject_profile. To run epi_reg with field maps, we need the effective echo spacing of the epi image being processed. The echo spacing should be listed in the parameters of the scan (e.g., 0.94 ms for my 32 direction B=1000 dti scans).  Divide the value by the grappa (acceleration) factor (2 in this case) and then divide by 1000 to get units in seconds instead of ms.
e.g., (0.94/2)/1000=0.00047

If you use dcm2niix with the -b option (for BIDS), then a *.json file is created for each image.
In this case, the effective echo spacing can be lifted directly from the json file like this:
-------
EffectiveEchoSpacing=`cat json/${epi_stem}.json | grep -m 1 EffectiveEchoSpacing | sed 's/,//g' | awk '{print $2}'`


######## REVERSE PHASE ENCODE DWI DATA ##############

DWI data are acquired in one direction (e.g., A->P).  Because the echo time for DWI images is so long, this means there is tremendous distortion.  If you look at A->P images in axial view, you'll see stretchy eyeballs.  If you acquire a second image (1-2 B0s) in the opposite phase-encode (P->A) this will take ~ 45 seconds.  This second image experiences tremendous distortion in the opposite direction of the original A->P images. If you look at the P->A image in axial view, you'll see it has squishy eyeballs.

Default Siemens A-P: stretchy eyeballs, (negative phase encode, blip down -1)
Reverse phase encode B0 P-A: squishy eyeballs, (positive phase encode, blip_up 1)

In FSL we can use topup to calculate the distortion parameters based on both images.
We can then use applytopup to apply these parameters to another image.
https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/topup/ApplyTopupUsersGuide

N.B. My experiments suggest it is better to correct the A-P data (less noise,
fewer spikes and holes in weird places, better eyeball reconstruction)

Finally, we feed the topup results into eddy for correcting eddy currents and movement.
https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/eddy

In bip/REF/DWI/ you will find the following parameter files needed for topup processing:
acqp_A-P.txt
acqparams.txt
index_A-P.txt

The acqp file is used by eddy, as described here:
http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/EDDY/UsersGuide
This is a text-file describing the acquisition parameters for the different images in
--imain. The format of this file is identical to that used by topup (though the
parameter is called --datain there).
The value is repeated 32 times for the 32 volumes.  The -1 indicated the phase
encode direction for y in A-P.  The last value ** Total Readout time for the Field maps ** is described below:

##### TOTAL READOUT TIME FOR THE FIELD MAPS #######
(Used by topup: appears in the acqp and acqparams files)
Total readout time is the echo spacing (dwell time) times the number of phase encoding steps.
If you use grappa, divide the number of phase encoding steps by the grappa factor.
echo time 0.94 ms (but we want it in seconds, so 0.00094 sec),
phase encoding steps=128,
grappa=2
e.g., (128/2)*.00094 sec =0.06016 sec
-------------
0 -1 0 0.0602 (acqp_A-P.txt...this line is repeated 32 times, once for each DWI volume)
-------------
acqparams.txt:  Used for topup.  Specifies the values for the 4 blip_down and
                blip_up B0 volumes
-------------
index_A-P.txt: Used by eddy to index all the values in acqp_A-P.txt (all 1's)
