#!/bin/bash
#.bip_profile takes an argument that sets the project specific profile.
# This way we can source the project specific profile with a variable name.

source ${PROJECT_PROFILE}
#==============================================================================

: <<COMMENTBLOCK

###############################################################################
AUTHOR:         Dianne Patterson University of Arizona
DATE CREATED:   Today is 7/18/12. It is now 3:14 PM (actually, it was started
                earlier in the month)
----------------------------  DEPENDENCIES  -----------------------------------
The script is dependent on 'profiles' found in /usr/local/tools/REF/PROFILES.
(e.g., bip_img_profile.sh, bip_subject_profile.sh and project specific profiles). The
profiles define variable names . All project specific profiles source
bip_subject_profile.sh which sources bip_img_profile.sh
===============================================================================
----------------------------  PURPOSE  ----------------------------------------
To identify the last iteration and keep going in the event of a breakdown in
running bip.sh
===============================================================================
----------------------------  INPUT  ------------------------------------------
A partially completed bip directory.
You must provide the tract name as an argument, e.g., bip_cont.sh Main arc5_l
from the top level subject dir.
===============================================================================
----------------------------  OUTPUT  -----------------------------------------
A finished bip dir if you run Main.
An evaluation of the volumes of the max pair if you run Eval.
A re-creation of the biplog file if you run BipLog.
===============================================================================
----------------------------  CALLS   -----------------------------------------
Form other scripts: FindMax from functions
###############################################################################

COMMENTBLOCK
###############################################################################
#########################  DEFINE FUNCTIONS  ##################################

: <<COMMENTBLOCK

Function:   BipLog
Purpose:    Create a fresh log file in the bip dir if there is a problem
            with the existing Log (saves the original biplog)
Input:      Needs a finished bip dir
Output:     biplog_${tract}.txt and old_biplog_${tract}.txt
Calls:      None

COMMENTBLOCK

BipLog ()
{
   tract_dir=${DWI_DERIV}/${tract}_bip                # Make sure tract_dir is defined
   mv ${tract_dir}/biplog_${tract}.txt ${tract_dir}/old_biplog_${tract}.txt
   touch ${tract_dir}/biplog_${tract}.txt       # Back up the log file
   seed=`head -1 ${tract_dir}/masks.txt`
   target=`tail -1 ${tract_dir}/masks.txt`
   seedvol=`fslstats ${SUBROIS}/${seed} -V | cut -d " " -f 1`
   echo "${seed} is the A series, size: ${seedvol}" >> ${tract_dir}/biplog_${tract}.txt
   targetvol=`fslstats ${SUBROIS}/${target} -V | cut -d " " -f 1`
   echo "${target} is the B series, size: ${targetvol}" >> ${tract_dir}/biplog_${tract}.txt
   # Get the max value
   FindMax ${tract_dir}/roi_A
   max=$i

   i=1
   j=2

while [ $j -le ${max} ]; do
  volA=`fslstats ${tract_dir}/roi_A_${i} -V | cut -d " " -f 1`
  echo "roi_A_${i} is ${volA}" >> ${tract_dir}/biplog_${tract}.txt
  volAA=`fslstats ${tract_dir}/roi_A_${j} -V | cut -d " " -f 1`
  echo "roi_A_${j} is ${volAA}" >> ${tract_dir}/biplog_${tract}.txt

  volB=`fslstats ${tract_dir}/roi_B_${i} -V | cut -d " " -f 1`
  echo "roi_B_${i} is ${volB}" >> ${tract_dir}/biplog_${tract}.txt
  volBB=`fslstats ${tract_dir}/roi_B_${j} -V | cut -d " " -f 1`
  echo "roi_B_${j} is ${volBB}" >> ${tract_dir}/biplog_${tract}.txt

  let i+=1
  let j+=1
done

}

#==============================================================================


: <<COMMENTBLOCK

Function:   BipSub
Purpose:    Do the heavy lifting of running probtrackx
Input:      <tract> <seed> <target> <output suffix for new seed roi>
            E.g.: $0 arc_l arc_l_bip/roi_A_1 arc_l_bip/roi_B_1 A_2
Output:     an iteration
Calls:      None

COMMENTBLOCK

BipSub_old ()
{

#tract=$1
seed=$2
target=$3
out=$4

echo "seed is ${seed}"
echo "target is ${target}"
pwd

touch ${tract_dir}/targets.txt
echo "${target}" > ${tract_dir}/targets.txt
#echo "BipSub thinks bedpost dir is ${BEDPOST}"

inv=${SUBROIS}/${tract}_inv_mask_diff_bin_csf

probtrackx --network --mode=seedmask -x ${seed}  -l -c 0.2 -S 2000 \
--steplength=0.5 -P 5000 --stop=${inv} --forcedir --opd \
-s ${BEDPOST}/merged -m ${BEDPOST}/nodif_brain_mask --dir=${tract_dir} \
--targetmasks=${tract_dir}/targets.txt --os2t

echo "size of old seed mask for ${tract} is"
fslstats ${seed} -V
echo "size of new seed mask ${tract} is"
fslstats ${tract_dir}/seeds* -V

mkdir ${tract_dir}/Iteration_${out}
immv ${tract_dir}/fdt_paths ${tract_dir}/Iteration_${out}
mv ${tract_dir}/targets.txt ${tract_dir}/Iteration_${out}
mv ${tract_dir}/*.log ${tract_dir}/Iteration_${out}
mv ${tract_dir}/waytotal ${tract_dir}/Iteration_${out}
cp ${tract_dir}/seeds* ${tract_dir}/Iteration_${out}
immv ${tract_dir}/seeds* ${tract_dir}/roi_${out}
fslmaths ${tract_dir}/roi_${out} -bin ${tract_dir}/roi_${out}
}

#==============================================================================
: <<COMMENTBLOCK

Function:   Eval
Purpose:    Identify current max roiA and roiB
            If the current max roi is not equal to the previous, then run another,
            If B is less than A, but B_current=B_previous, then copy B_current to B_next
Input:      tract, e.g., arc5_l
Output:     Spits out volumes of last 2 rois A and B.  Allows one to quickly determine
            what tracts finished correctly (the 2 values should be the same if the bip
            process is done)
Calls:      GetIJ Evalreport (in this script)

COMMENTBLOCK

Eval ()
{
GetIJ
EvalReport
}

#==============================================================================
: <<COMMENTBLOCK

Function:   EvalReport
Purpose:    Provide values for user to decide whether or not to run bip_cont.sh
Input:      Assumes a tract is specified
Output:     Standard out (maybe a txt file)
Calls:      None

COMMENTBLOCK

EvalReport ()
{
if [ -e  ${tract_dir}/roi_A_1.nii.gz ]; then
  # previous volumes
  if [ ${iA} -gt 1 ]; then
    volhA=`fslstats ${tract_dir}/roi_A_${hA} -V | cut -d " " -f 1`
  fi

  if [ ${iB} -gt 1 ]; then
    volhB=`fslstats ${tract_dir}/roi_B_${hB} -V | cut -d " " -f 1`
  fi
  # current volumes
  volA=`fslstats ${tract_dir}/roi_A_${iA} -V | cut -d " " -f 1`
  volB=`fslstats ${tract_dir}/roi_B_${iB} -V | cut -d " " -f 1`
  echo "tract is ${tract} in ${tract_dir}"

  if [ ${iA} -gt 1 ]; then
    echo "roiA_${hA}: Vol=${volhA}"
  fi
    echo "roiA_${iA}: Vol=${volA}"
    echo

  if [ ${iB} -gt 1 ]; then
    echo "roiB_${hB}: Vol=${volhB}"
  fi
    echo "roiB_${iB}: Vol=${volB}"
  elif [ -e ${tract_dir}/biplog* ]; then
    echo
    echo "!!At least the log exists!!"
    echo
fi
}


#==============================================================================
: <<COMMENTBLOCK

Function:   GetIJ
Purpose:    Get the values of i and j
Input:      A tract (this input will identify the tract directory, given the
            standard structure).
Output:     i and j
Calls:      FindMax from bip_functions.sh

COMMENTBLOCK

GetIJ ()
{
tract_dir=${DWI_DERIV}/${tract}_bip
# Find iA and jA for roiA
FindMax ${tract_dir}/roi_A
iA=${i}
if [ ${iA} -gt 1 ]; then
  let "hA= ${iA} - 1"
fi
let "jA= ${iA} + 1"
# echo "iA is ${iA}; jA is ${jA}"

# Find iB and jB for roiB
FindMax ${tract_dir}/roi_B
iB=${i}
if [ ${iB} -gt 1 ]; then
  let "hB= ${iB} - 1"
fi
let "jB= ${iB} + 1"
# echo "iB is ${iB}; jB is ${jB}"
}

#==============================================================================

: <<COMMENTBLOCK

Function:   GetVols
Purpose:    Get volumes for comparisons and logs
Input:      Masks
Output:     Variables volA volAA volB volBB
Calls:      None

COMMENTBLOCK

GetVols ()
{
volA=`fslstats ${tract_dir}/roi_A_${iA} -V | cut -d " " -f 1`
volAA=`fslstats ${tract_dir}/roi_A_${jA} -V | cut -d " " -f 1`
volB=`fslstats ${tract_dir}/roi_B_${iB} -V | cut -d " " -f 1`
volBB=`fslstats ${tract_dir}/roi_B_${jB} -V | cut -d " " -f 1`
}

#==============================================================================
: <<COMMENTBLOCK

Function:   Iterate
Purpose:    Run probtrack iteratively until endpoint size stabilizes
Input:      Directory with initial iterations done
Output:     Finished dir and images
Calls:      BipSub in bip.sh; GetIJ, GetVols & Stats in this script

COMMENTBLOCK

Iterate ()
{
echo "run first bip round"
echo "tract: ${tract}"
echo "roiA: ${tract_dir}/roi_A_${iA}"
echo "roiB: ${tract_dir}/roi_B_${iB}"
BipSub ${tract} ${tract_dir}/roi_A_${iA} ${tract_dir}/roi_B_${iB} A_${jA}

echo "run 2nd bip round"
BipSub ${tract} ${tract_dir}/roi_B_${iB} ${tract_dir}/roi_A_${jA} B_${jB}

GetVols
echo "increment i and j"
GetIJ

while [ $volA !=  $volAA -o  $volB !=  $volBB ]; do
  echo "run first BipSub round in loop"
  if [ $volA !=  $volAA ]; then
    BipSub ${tract} ${tract_dir}/roi_A_${iA} ${tract_dir}/roi_B_${iB} A_${jA}
  elif [ $volA =  $volAA ]; then
    imcp ${tract_dir}/roi_A_${iA} ${tract_dir}/roi_A_${jA}
  fi

  echo "run 2nd BipSub round in loop"
  if [ $volB !=  $volBB ]; then
    BipSub ${tract} ${tract_dir}/roi_B_${iB} ${tract_dir}/roi_A_${jA} B_${jB}
  elif [ $volB =  $volBB ]; then
    imcp ${tract_dir}/roi_B_${iB} ${tract_dir}/roi_B_${jB}
  fi

  GetVols
  Stats

  echo "increment i and j"
  GetIJ
done

}




#==============================================================================
: <<COMMENTBLOCK

Function:   Stats
Purpose:    Log volumes
Input:      Masks to be measured
Output:     Log file entries
Calls:      None

COMMENTBLOCK

Stats ()
{
#         echo "roi_A_${i} is ${volA}"
#         echo "roi_A_${j} is ${volAA}"
#         echo "roi_B_${i} is ${volB}"
#         echo "roi_B_${j} is ${volBB}"

echo "put stats values in log in loop"
echo "roi_A_${iA} is ${volA}" >> ${tract_dir}/biplog_${tract}.txt
echo "roi_A_${jA} is ${volAA}" >> ${tract_dir}/biplog_${tract}.txt
echo "roi_B_${iA} is ${volB}" >> ${tract_dir}/biplog_${tract}.txt
echo "roi_B_${jA} is ${volBB}" >> ${tract_dir}/biplog_${tract}.txt
}


 #==============================================================================

: <<COMMENTBLOCK

Function:   Max
Purpose:    Identify current max roiA and roiB and their volumes
Input:      Tract, e.g., arc5_l
Output:     Max iterations A and B and volume in voxels
			      This is specifically to address questions by reviewer 2 about the
			      number of iterations needed to reach stability.
Calls:      FindMax in bip_functions.sh

COMMENTBLOCK

Max ()
{

tract_dir=${DWI_DERIV}/${tract}_bip
# Find iA and jA for roiA
FindMax ${tract_dir}/roi_A
iA=${i}
volA=`fslstats ${tract_dir}/roi_A_${iA} -V | cut -d " " -f 1`

# Find iB and jB for roiB
FindMax ${tract_dir}/roi_B
iB=${i}
volB=`fslstats ${tract_dir}/roi_B_${iB} -V | cut -d " " -f 1`
#echo "tract iter_A volA iter_B volB"
echo "${SUBJECT} ${tract} ${iA} ${volA} ${iB} ${volB}"

}

 #==============================================================================
: <<COMMENTBLOCK

Function:   SeedSize
Purpose:    Determine whether size of seed influences # of iterations
Input:      Tract name (e.g., arc5_l)
Output:     Output
Calls:      None

COMMENTBLOCK

SeedSize ()
{

touch seedsize.txt
tract=$1
tract_dir=${DWI_DERIV}/${tract}_bip

if [ ! -d ${tract_dir} ]; then
  echo "directory does not exist"
  echo "Please run bip_prep.sh"
  exit 1
fi

roi1=`head -n 1 ${tract_dir}/masks.txt`
roi2=`tail -n 1 ${tract_dir}/masks.txt`

pwd
volA=`fslstats ${SUBROIS}/${roi1} -V | cut -d " " -f 1`
volB=`fslstats ${SUBROIS}/${roi2} -V | cut -d " " -f 1`
echo "${SUBJECT} ${roi1} ${volA}" >>seedsize.txt
echo "${SUBJECT} ${roi2} ${volB}" >>seedsize.txt

}

#==============================================================================
: <<COMMENTBLOCK

Function:   HelpMessage
Purpose:    Display help if user enters fewer than one argument
Input:      None
Output:     Help Message
Calls:      None

COMMENTBLOCK

HelpMessage ()
{
echo "Evaluation Usage:"
echo "Usage: $0 Eval <tract>"
echo "Example: $0 Eval cst_l"
echo "This will generate a short report"
echo "allowing the user to decide if iteration is needed"
echo "if the current and previos volumes are the same for both"
echo "roiA and roiB, then the tract is finished"
echo
echo "Usage: $0 Main <tract>"
echo "Example: $0 Main cst_l"
echo "This will start the iteration procedure"
echo
echo "Usage: $0 BipLog <tract>"
echo "Example: $0 BipLog cst_l"
echo "This will start recreate the log file from scratch."
echo
echo "Usage: $0 Max <tract>"
echo "Example: $0 Max cst_l"
echo "this will report the last iteration and voxel count for each subject"

exit 1
}

#==============================================================================

: <<COMMENTBLOCK

Function:   Main
Purpose:    Identify current max roiA and roiB
            If the current max roi is not equal to the previous, then run another,
            If B is less than A, but B_current=B_previous, then copy B_current to B_next
Input:      Tract, e.g., arc5_l
Output:
Calls:      GetIJ and Iterate from this script

COMMENTBLOCK

Main ()
{
echo "${tract}"
GetIJ
Iterate
}


#########################  END FUNCTION DEFINITIONS  ##########################
###############################################################################


if [ $# -lt 1 ]; then
  HelpMessage
fi

while getopts h options 2> /dev/null; do        # Setup -h flag. redirect errors
  case $options in                            			# In case someone invokes -h
  h) HelpMessage;;                            			# Run the help message function
  \?) echo "only h is a valid flag" 1>&2   		# If bad options are passed, print message
  esac
done

shift $(( $OPTIND -1))                          		# Index option count, so h isn't treated as arg
optnum=$(( $OPTIND -1))                         	# optnums hold # of options passed in

if [ ${optnum} -lt 1 -a $# -gt 1 ]; then        	# If no options
  dothis=$1                                       			# then first arg is function
  tract=$2                                        			# 2nd arg is tract
  $dothis ${tract}                                			# Execute requested function
fi

MyLog                                           				# Log script, args & date
